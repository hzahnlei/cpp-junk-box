/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/convenient_algorithms.hpp"
#include <catch2/catch_all.hpp>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>

using std::less;
using std::make_shared;
using std::make_unique;
using std::ostringstream;
using std::set;
using std::shared_ptr;
using std::string;
using std::unique_ptr;
using std::vector;

using namespace junkbox;

TEST_CASE("all_of is only true if condition met by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{1, 2, 3};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::all_of(some_numbers, is_positive_number);

	REQUIRE(actual == true);
	REQUIRE(loop_count == 3);
	REQUIRE(result.str() == "123");
}

TEST_CASE("all_of is false if condition violated by a single collection element.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{1, -2, 3};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::all_of(some_numbers, is_positive_number);

	REQUIRE(actual == false);
	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "1-2");
}

TEST_CASE("any_of is true if condition met by a single collection element.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{-1, 2, -3};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::any_of(some_numbers, is_positive_number);

	REQUIRE(actual == true);
	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "-12");
}

TEST_CASE("any_of is false if condition violated by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{-1, -2, -3};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::any_of(some_numbers, is_positive_number);

	REQUIRE(actual == false);
	REQUIRE(loop_count == 3);
	REQUIRE(result.str() == "-1-2-3");
}

TEST_CASE("none_of is true if condition violated by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{-1, -2, -3};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::none_of(some_numbers, is_positive_number);

	REQUIRE(actual == true);
	REQUIRE(loop_count == 3);
	REQUIRE(result.str() == "-1-2-3");
}

TEST_CASE("none_of is false if condition met by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{-1, 2, -3};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::none_of(some_numbers, is_positive_number);

	REQUIRE(actual == false);
	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "-12");
}

TEST_CASE("contains is true if collection contains given element.", "[UT, fast]")
{
	const vector<int> some_numbers{1, 2, 3};

	const auto actual = algo::contains(some_numbers, 2);

	REQUIRE(actual == true);
}

TEST_CASE("contains is false if collection does not contain given element.", "[UT, fast]")
{
	const vector<int> some_numbers{1, 2, 3};

	const auto actual = algo::contains(some_numbers, 4711);

	REQUIRE(actual == false);
}

TEST_CASE("for_each iterates all elements in order and exactly once.", "[UT, fast]")
{
	ostringstream result;
	const vector<string> some_strings{"hello", "world"};
	auto loop_count = 0;

	algo::for_each(some_strings,
	               [&](const auto &some_string)
	               {
			       loop_count++;
			       result << some_string << '\n';
		       });

	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "hello\nworld\n");
}

TEST_CASE("for_each also works if elements are unique_ptr.", "[UT, fast]")
{
	ostringstream result;
	// This would be a nice place for using collection::as_vector.
	vector<unique_ptr<string>> some_strings;
	some_strings.emplace_back(make_unique<string>("hello"));
	some_strings.emplace_back(make_unique<string>("world"));
	auto loop_count = 0;

	// some_string is a reference to a unique_ptr.
	algo::for_each(some_strings,
	               [&](const auto &some_string)
	               {
			       loop_count++;
			       result << *some_string << '\n';
		       });

	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "hello\nworld\n");
	REQUIRE(*some_strings[0] == "hello");
	REQUIRE(*some_strings[1] == "world");
}

TEST_CASE("transform transforms all elements in order and exactly once.", "[UT, fast]")
{
	// Cannot be "const" because we are modifying it.
	vector<int> some_numbers{1, 2, 3};
	auto loop_count = 0;
	const auto negate = [&](const auto &some_number)
	{
		loop_count++;
		return -some_number;
	};

	algo::transform(some_numbers, negate);

	REQUIRE(loop_count == 3);
	REQUIRE(some_numbers.size() == 3);
	REQUIRE(some_numbers[0] == -1);
	REQUIRE(some_numbers[1] == -2);
	REQUIRE(some_numbers[2] == -3);
}

TEST_CASE("copy_if returns a collection that only contains the elements fulfilling the given criterion.", "[UT, fast]")
{
	ostringstream result;
	const vector<int> some_numbers{1, -2, 3, -4, 5, -6};
	auto loop_count = 0;
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = algo::copy_if(some_numbers, is_positive_number);

	REQUIRE(loop_count == 6);
	REQUIRE(result.str() == "1-23-45-6");
	REQUIRE(some_numbers.size() == 6);
	REQUIRE(some_numbers[0] == 1);
	REQUIRE(some_numbers[1] == -2);
	REQUIRE(some_numbers[2] == 3);
	REQUIRE(some_numbers[3] == -4);
	REQUIRE(some_numbers[4] == 5);
	REQUIRE(some_numbers[5] == -6);
	REQUIRE(actual.size() == 3);
	REQUIRE(actual[0] == 1);
	REQUIRE(actual[1] == 3);
	REQUIRE(actual[2] == 5);
}

TEST_CASE("copy_if works with shared_ptr (but not with unique_ptr).", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1),  make_shared<int>(-2), make_shared<int>(3),
	                                           make_shared<int>(-4), make_shared<int>(5),  make_shared<int>(-6)};
	auto loop_count = 0;
	// n is a reference to a shared_ptr.
	const auto is_positive_number = [&](const auto &n)
	{
		loop_count++;

		result << *n;
		return *n >= 0;
	};

	const auto actual = algo::copy_if(some_numbers, is_positive_number);

	REQUIRE(loop_count == 6);
	REQUIRE(result.str() == "1-23-45-6");
	REQUIRE(some_numbers.size() == 6);
	REQUIRE(*some_numbers[0] == 1);
	REQUIRE(*some_numbers[1] == -2);
	REQUIRE(*some_numbers[2] == 3);
	REQUIRE(*some_numbers[3] == -4);
	REQUIRE(*some_numbers[4] == 5);
	REQUIRE(*some_numbers[5] == -6);
	REQUIRE(actual.size() == 3);
	REQUIRE(*actual[0] == 1);
	REQUIRE(*actual[1] == 3);
	REQUIRE(*actual[2] == 5);
}

TEST_CASE("'Hello' appears zero times in empty vector.", "[UT, fast]")
{
	const vector<string> empty_collection;
	REQUIRE(algo::count_of_occurrences(empty_collection, "Hello") == 0);
}

TEST_CASE("'Hello' appears zero times in empty set.", "[UT, fast]")
{
	const set<string, less<>> empty_collection;
	REQUIRE(algo::count_of_occurrences(empty_collection, "Hello") == 0);
}

TEST_CASE("'Hello' appears zero times in vector ['abc', 'xyz'].", "[UT, fast]")
{
	const vector<string> collection{"abc", "xyz"};
	REQUIRE(algo::count_of_occurrences(collection, "Hello") == 0);
}

TEST_CASE("'Hello' appears zero times in set {'abc', 'xyz'}.", "[UT, fast]")
{
	const set<string, less<>> collection{"abc", "xyz"};
	REQUIRE(algo::count_of_occurrences(collection, "Hello") == 0);
}

TEST_CASE("'Hello' appears one times in vector ['abc', 'Hello', 'xyz'].", "[UT, fast]")
{
	const vector<string> collection{"abc", "Hello", "xyz"};
	REQUIRE(algo::count_of_occurrences(collection, "Hello") == 1);
}

TEST_CASE("'Hello' appears one times in set {'abc', 'Hello', 'xyz'}.", "[UT, fast]")
{
	const set<string, less<>> collection{"abc", "Hello", "xyz"};
	REQUIRE(algo::count_of_occurrences(collection, "Hello") == 1);
}

TEST_CASE("'Hello' appears two times in vector ['abc', 'Hello', 'xyz', 'Hello'].", "[UT, fast]")
{
	const vector<string> collection{"abc", "Hello", "xyz", "Hello"};
	REQUIRE(algo::count_of_occurrences(collection, "Hello") == 2);
}

TEST_CASE("'Hello' appears one times in set {'abc', 'Hello', 'xyz', 'Hello'}.", "[UT, fast]")
{
	const set<string, less<>> collection{"abc", "Hello", "xyz", "Hello"};
	REQUIRE(algo::count_of_occurrences(collection, "Hello") == 1);
}
