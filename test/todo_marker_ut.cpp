/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/todo_marker.hpp"
#include <catch2/catch_all.hpp>

using Catch::Matchers::EndsWith;
using Catch::Matchers::StartsWith;

TEST_CASE("Todo macro can be used for pathes we still need to finish.", "[UT, fast]")
{
	try
	{
		TODO();
		// cppcheck-suppress unreachableCode
		FAIL("Expected exception.");
	}
	catch (const junkbox::To_Do &cause)
	{
		REQUIRE_THAT(cause.what(), StartsWith("TODO in"));
		REQUIRE_THAT(cause.what(), EndsWith("todo_marker_ut.cpp at 17"));
	}
}

TEST_CASE("Not-supported macro can be used for pathes we still need to be "
          "finished.",
          "[UT, fast]")
{
	try
	{
		NOT_SUPPORTED();
		// cppcheck-suppress unreachableCode
		FAIL("Expected exception.");
	}
	catch (const junkbox::Not_Supported &cause)
	{
		REQUIRE_THAT(cause.what(), StartsWith("Unsupported operation in"));
		REQUIRE_THAT(cause.what(), EndsWith("todo_marker_ut.cpp at 34"));
	}
}

TEST_CASE("Runtime-error macro can be used to signal severe problems, "
          "including file and line.",
          "[UT, fast]")
{
	try
	{
		RUNTIME_ERROR("Some unexpected condition.");
		// cppcheck-suppress unreachableCode
		FAIL("Expected exception.");
	}
	catch (const junkbox::Runtime_Error &cause)
	{
		REQUIRE_THAT(cause.what(), StartsWith("Error in"));
		REQUIRE_THAT(cause.what(), EndsWith("Some unexpected condition."));
	}
}

TEST_CASE("Messages can be complex expressions.", "[UT, fast]")
{
	try
	{
		RUNTIME_ERROR("Some unexpected condition. Char " + static_cast<char>(123) + " not allowed.");
		// cppcheck-suppress unreachableCode
		FAIL("Expected exception.");
	}
	catch (const junkbox::Runtime_Error &cause)
	{
		REQUIRE_THAT(cause.what(), StartsWith("Error in"));
		REQUIRE_THAT(cause.what(), EndsWith("Some unexpected condition. Char { not "
		                                    "allowed."));
	}
}
