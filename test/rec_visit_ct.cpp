/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/rec_visit.hpp"
#include <catch2/catch_all.hpp>
#include <ostream>
#include <sstream>
#include <string>
#include <variant>

using namespace std;
using namespace junkbox;

//---- Data structures to represent expressions -------------------------------

/*
 * Not recursive and therefore does not have to be declared forward.
 */
struct Int_Literal final
{
	int value;
};

/*
 * Recursive and therefore have to be declared forward.
 */
struct Add_Op;
struct Mul_Op;

/*
 * See how we box the incomplete (forward declared) types here. With the boxing we avoid compile time errors, as variant
 * wont accept incomplete types.
 */
using Expr = variant<Int_Literal, Box<Add_Op>, Box<Mul_Op>>;

struct Add_Op final
{
	Expr lhs;      // Now we can refer to the variant type.
	Box<Expr> rhs; // We could box is, but we don't have to
};

struct Mul_Op final
{
	Expr lhs;
	Expr rhs;
};

//---- Test with closures and visitor that returns a value --------------------

auto eval(const Expr &expression) -> int
{
	return visit(overloaded{[](const Int_Literal &literal) { return literal.value; },
	                        [](const Add_Op &op) { return eval(op.lhs) + eval(op.rhs); },
	                        [](const Mul_Op &op) { return eval(op.lhs) * eval(op.rhs); }},
	             expression);
};

TEST_CASE("Visitor works for types with recursive references.", "[UT, fast]")
{
	GIVEN("Structure of expressions")
	{
		// Here the "auto-boxing" makes life easier for us. We do not need to explicitly box everything. The
		// constructors have explicitly NOT been marked as "explicit".
		const auto expression = Expr{
				Add_Op{.lhs = Expr{Int_Literal{1}},
		                       .rhs = Expr{Mul_Op{.lhs = Expr{Int_Literal{2}}, .rhs = Expr{Int_Literal{3}}}}}};
		WHEN("evaluating this expression")
		{
			const auto result = eval(expression);
			THEN("the correct result is computed") { REQUIRE(result == 7); }
		}
	}
}

//---- Test with closures and visitor that does not return a value ------------

auto print(const Expr &expression, ostream &out) -> void
{
	return visit(overloaded{[&out](const Int_Literal &literal) { out << literal.value; },
	                        [&out](const Add_Op &op)
	                        {
					print(op.lhs, out);
					out << " + ";
					print(op.rhs, out);
				},
	                        [&out](const Mul_Op &op)
	                        {
					out << '(';
					print(op.lhs, out);
					out << " * ";
					print(op.rhs, out);
					out << ')';
				}},
	             expression);
};

TEST_CASE("Visitor do not have to return values.", "[UT, fast]")
{
	GIVEN("Structure of expressions")
	{
		const auto expression = Expr{
				Add_Op{.lhs = Expr{Int_Literal{2}},
		                       .rhs = Expr{Mul_Op{.lhs = Expr{Int_Literal{3}}, .rhs = Expr{Int_Literal{4}}}}}};
		ostringstream result;
		WHEN("printing this expression")
		{
			print(expression, result);
			THEN("the expression structure is") { REQUIRE(result.str() == "2 + (3 * 4)"); }
		}
	}
}

//---- Test with an struct instead of closures --------------------------------

struct Printing_Visitor
{
	ostream &out;
	auto operator()(const Int_Literal &literal) -> void { out << literal.value; }
	auto operator()(const Add_Op &op) -> void
	{
		out << "(ADD ";
		visit(*this, op.lhs); // This member was not boxed.
		out << ' ';
		visit(*this, op.rhs.unboxed()); // This member was boxed.
		out << ')';
	}
	auto operator()(const Mul_Op &op) -> void
	{
		out << "(MUL ";
		visit(*this, op.lhs);
		out << ' ';
		visit(*this, op.rhs);
		out << ')';
	}
};

TEST_CASE("Explicit visitor struct", "[UT, fast]")
{
	GIVEN("Structure of expressions")
	{
		const auto expression = Expr{
				Add_Op{.lhs = Expr{Int_Literal{2}},
		                       .rhs = Expr{Mul_Op{.lhs = Expr{Int_Literal{3}}, .rhs = Expr{Int_Literal{4}}}}}};
		ostringstream result;
		WHEN("printing this expression unsing the visitor")
		{
			visit(Printing_Visitor{result}, expression);
			THEN("the expression structure is") { REQUIRE(result.str() == "(ADD 2 (MUL 3 4))"); }
		}
	}
}

//---- Test where all objects are part of an iheritance hierarchy. ------------
//---- And you do not want to use std::visit but call methods from ------------
//---- the base class. --------------------------------------------------------

// Forward declaration again.
struct Natural_Entity;
struct Legal_Entity;

// The variant. Only used as a reference type. But std::visit not used.
using Entity = variant<Box<Natural_Entity>, Box<Legal_Entity>>;

// The base class with a print method we want to call.
struct Base_Entity
{
	virtual ~Base_Entity() = default;
	virtual auto print(ostream &out) const -> void = 0;
};

struct Natural_Entity final : public Base_Entity
{
	string first_name;
	string last_name;
	string born_in;
	Entity is_working_for;
	Natural_Entity(const string &first, const string &last, const string &born, const Entity &employer)
			: first_name{first}, last_name{last}, born_in{born}, is_working_for{employer}
	{
	}
	auto print(ostream &out) const -> void override
	{
		out << first_name << ' ' << last_name << " (*" << born_in << "), working for ";
		// Using "as" function to extract the value from the variant, no matter what type. Values are indeed
		// return as a reference to their base type.
		unboxed_as<Base_Entity>(is_working_for).print(out);
	}
};

struct Legal_Entity final : public Base_Entity
{
	string name;
	string founded_in;
	vector<Entity> is_owned_by;
	Legal_Entity(const string &name, const string &founded, vector<Entity> &&owned)
			: name{name}, founded_in{founded}, is_owned_by{move(owned)}
	{
	}
	auto print(ostream &out) const -> void override
	{
		out << name << " (est. " << founded_in << "), owned by: ";
		auto i = 0U;
		for (const auto &owner : is_owned_by)
		{
			if (i > 0)
				out << ", ";
			unboxed_as<Base_Entity>(owner).print(out);
		}
	}
};

TEST_CASE("Object can be unboxed from std::variant as ref to base class", "[UT, fast]")
{
	GIVEN("Structure of expressions")
	{
		const Natural_Entity john_doe{"John", "Doe", "2005-06-24",
		                              Legal_Entity{"ACME", "1984",
		                                           vector<Entity>{Legal_Entity{"Angel Investments", "1975",
		                                                                       vector<Entity>{}}}}};
		ostringstream result;
		WHEN("printing this expression unsing the visitor")
		{
			john_doe.print(result);
			THEN("the expression structure is")
			{
				REQUIRE(result.str() == "John Doe (*2005-06-24), working for ACME (est. 1984), owned "
				                        "by: Angel Investments (est. 1975), owned by: ");
			}
		}
	}
}

//---- Compare overloaded and overloaded_rec ----------------------------------

struct Bool_Type;
struct Float_Type;
struct Int_Type;

using Type_Info = variant<Box<Bool_Type>, Box<Float_Type>, Box<Int_Type>>;

struct Bool_Type final
{
	size_t bit_count;
};
struct Float_Type final
{
	size_t bit_count;
};
struct Int_Type final
{
	size_t bit_count;
};

TEST_CASE("overloaded does not resolve type correctly", "[UT, fast]")
{
	GIVEN("non-exhaustive overloaded")
	{
		const auto non_exhaustive = overloaded{
				[](const Int_Type &) { return "Int_Type"; },
				[](const auto &) { return "auto"; },
		};
		const auto type_info = Type_Info{Int_Type{.bit_count = 32U}};
		WHEN("applying std::visit")
		{
			const auto result = visit(non_exhaustive, type_info);
			THEN("default is matched instead of expected type") { REQUIRE(result == "auto"s); }
		}
	}
}

/*
 * Originally const overloaded_rec did always choose the default case (auto) in non-exhaustive overloads. This has been
 * fixed and this test is here to prove it. (Below is a test to make sure the non-const case still works too.)
 */
TEST_CASE("overloaded_rec resolves type correctly", "[UT, fast]")
{
	GIVEN("non-exhaustive overloaded_rec")
	{
		const auto non_exhaustive = overloaded_rec{
				[](const Int_Type &) { return "Int_Type"; },
				[](const auto &) { return "auto"; },
		};
		const auto type_info = Type_Info{Int_Type{.bit_count = 32U}};
		WHEN("applying std::visit")
		{
			const auto result = visit(non_exhaustive, type_info);
			THEN("correct type is matched") { REQUIRE(result == "Int_Type"s); }
		}
	}
}

TEST_CASE("We can get overloaded to work by using Box and unboxed", "[UT, fast]")
{
	GIVEN("non-exhaustive overloaded")
	{
		const overloaded non_exhaustive{
				[](const Box<Int_Type> &type_info)
				{ return "Int_Type/"s + to_string(type_info.unboxed().bit_count); },
				[](const auto &) { return "auto"s; },
		};
		const Type_Info type_info{Int_Type{.bit_count = 32U}};
		WHEN("applying std::visit")
		{
			const auto result = visit(non_exhaustive, type_info);
			THEN("default is matched instead of expected type") { REQUIRE(result == "Int_Type/32"s); }
		}
	}
}

/*
 * Fixed a bug where const overloaded_rec did always choose the default case (auto) in non-exhaustive overloads. This
 * test makes sure, overloaded_rec still works when it is not const.
 */
TEST_CASE("overloaded_rec resolves type correctly, even if it is non-const", "[UT, fast]")
{
	GIVEN("non-exhaustive overloaded_rec")
	{
		overloaded_rec non_exhaustive{
				[](const Int_Type &) { return "Int_Type"; },
				[](const auto &) { return "auto"; },
		};
		const auto type_info = Type_Info{Int_Type{.bit_count = 32U}};
		WHEN("applying std::visit")
		{
			const auto result = visit(non_exhaustive, type_info);
			THEN("correct type is matched") { REQUIRE(result == "Int_Type"s); }
		}
	}
}
