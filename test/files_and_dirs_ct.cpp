/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/files_and_dirs.hpp"
#include "test_tools/files_and_dirs_test_fixture.hpp"
#include <catch2/catch_all.hpp>
#include <string>

using namespace std::literals::string_literals;
using namespace junkbox;

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture, "directory_exists is true if directory actually exists.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");

	const auto actual = when_we_apply(file::directory_exists).on_directory("/tmp/file_test");

	then_we_expect(actual).to_be(true);
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture, "directory_exists is false if directory does not exist.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");

	const auto actual = when_we_apply(file::directory_exists)
	                                    .on_directory("/tmp/file_test/"
	                                                  "nonexsistent");

	then_we_expect(actual).to_be(false);
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture,
                 "File count is 2 for empty directory (because of '.' and "
                 "'..').",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");

	const auto actual = when_we_run(file::file_count).on_given_directory("/tmp/file_test");

	then_we_expect(actual).to_be(static_cast<size_t>(2));
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture, "File count is 5 for directory with 3 files plus '.' and '..'.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");
	and_a_file_is_stored_in_that_directory("test1.txt");
	and_a_file_is_stored_in_that_directory("test2.txt");
	and_a_file_is_stored_in_that_directory("test3.txt");

	const auto actual = when_we_run(file::file_count).on_given_directory("/tmp/file_test");

	then_we_expect(actual).to_be(static_cast<size_t>(5));
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture,
                 "Attempt to count files of nonexisting directory results in "
                 "nice error message.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");

	when_we_run(file::file_count).on_given_nonexistent_directory("/tmp/file_test/doesnotexist");

	then_this_should_fail();
	and_we_expect_cause().to_be(
			"OS error code #2 (No such file or directory): Failed to count files in directory '/tmp/file_test/doesnotexist'."s);
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture, "All files can be deleted from given directory.", "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");
	and_a_file_is_stored_in_that_directory("test1.txt");
	and_a_file_is_stored_in_that_directory("test2.txt");
	and_a_file_is_stored_in_that_directory("test3.txt");

	when_we_call(file::delete_files_from_directory).on_that_directory();

	then_that_directory_should_still_exist();
	but_all_files_in_that_directory_should_be_gone();
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture,
                 "Attemnpt to delete files from nonexistent directory has no "
                 "effect.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");

	when_we_call(file::delete_files_from_directory).on_nonexisting_directory("/tmp/file_test/does_not_exist");

	then_this_should_succeed().without_any_effect();
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture,
                 "All files can be deleted from given directory, but "
                 "subdirectories still exist.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");
	and_a_file_is_stored_in_that_directory("test1.txt");
	and_a_file_is_stored_in_that_directory("test2.txt");
	and_a_file_is_stored_in_that_directory("test3.txt");
	given_empty_sub_directory("subfolder");
	and_a_file_is_stored_in_that_sub_directory("test4.txt");
	and_a_file_is_stored_in_that_sub_directory("test5.txt");

	when_we_call(file::delete_files_from_directory).on_that_directory();

	then_that_directory_should_still_exist();
	but_all_files_in_that_parent_directory_should_be_gone();
	and_that_sub_directory_should_still_exist();
	but_all_files_in_that_sub_directory_should_be_gone();
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture,
                 "delete_all_content_from_directory completely empties a "
                 "directory including sub-directories.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");
	and_a_file_is_stored_in_that_directory("test1.txt");
	and_a_file_is_stored_in_that_directory("test2.txt");
	and_a_file_is_stored_in_that_directory("test3.txt");
	given_empty_sub_directory("subfolder");
	and_a_file_is_stored_in_that_sub_directory("test4.txt");
	and_a_file_is_stored_in_that_sub_directory("test5.txt");

	when_we_call(file::delete_all_content_from_directory).on_that_directory();

	then_that_directory_should_still_exist();
	but_all_files_in_that_directory_should_be_gone();
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture, "Content of text file can be loaded.", "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");
	and_a_file_is_stored_in_that_directory("file_with_content.txt", "*** some content ***");

	const auto actually_loaded_content = when_we_invoke(file::load_text_file)
	                                                     .on_file("/tmp/file_test/"
	                                                              "file_with_content.txt");

	then_we_expect(actually_loaded_content).to_be("*** some content ***"s);
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture, "Line breaks of text file are keept when loaded.", "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");
	and_a_file_is_stored_in_that_directory("file_with_content.txt", "1st line\n2nd line");

	const auto actually_loaded_content = when_we_invoke(file::load_text_file)
	                                                     .on_file("/tmp/file_test/"
	                                                              "file_with_content.txt");

	then_we_expect(actually_loaded_content).to_be("1st line\n2nd line"s);
}

TEST_CASE_METHOD(test::Files_And_Dirs_Test_Fixture,
                 "Explanatory error message will be issued if file does not "
                 "exist.",
                 "[CT, fast]")
{
	given_empty_directory("/tmp/file_test");

	when_we_invoke(file::load_text_file).on_nonexisting_file("/tmp/file_test/does_not_exist.txt");

	then_this_should_fail();
	and_we_expect_cause().to_be(
			"OS error code #2 (No such file or directory): Failed to open file '/tmp/file_test/does_not_exist.txt'."s);
}
