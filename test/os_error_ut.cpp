/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/os_error.hpp"
#include <catch2/catch_all.hpp>
#include <fstream>
#include <string>

using std::ifstream;
using std::string;

using namespace junkbox;

TEST_CASE("Error message should indicate no error if nothing bad had happened.", "[UT, fast]")
{
	errno = 0;
	REQUIRE(os_error_message("Hello") == "OS error code #0 (Success): Hello");
}

TEST_CASE("Error message should indicate an error if something bad had "
          "happened.",
          "[UT, fast]")
{
	const string name{"This file for sure does not exist."};
	ifstream some_file{name};
	REQUIRE(os_error_message(name) == "OS error code #2 (No such file or directory): This file for "
	                                  "sure does not exist.");
}
