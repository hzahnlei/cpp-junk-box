/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/deref_algorithms.hpp"
#include <catch2/catch_all.hpp>
#include <memory>
#include <sstream>
#include <vector>

using std::make_shared;
using std::ostringstream;
using std::shared_ptr;
using std::vector;

using namespace junkbox;

TEST_CASE("Dereferencing all_of is only true if condition met by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	auto loop_count = 0;
	// Notice how that lambda gets passed in an integer reference, not a smart pointer. Even though
	// passing references to primitive types makes no sense. This is just for simplicity here.
	const auto is_positive_number = [&](const int &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = deref::all_of(some_numbers, is_positive_number);

	REQUIRE(actual == true);
	REQUIRE(loop_count == 3);
	REQUIRE(result.str() == "123");
}

TEST_CASE("Dereferencing all_of is false if condition violated by a single collection element.", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(-2), make_shared<int>(3)};
	auto loop_count = 0;
	// Notice how that lambda gets passed in an integer reference, not a smart pointer. Even though
	// passing references to primitive types makes no sense. This is just for simplicity here.
	const auto is_positive_number = [&](const int &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = deref::all_of(some_numbers, is_positive_number);

	REQUIRE(actual == false);
	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "1-2");
}

TEST_CASE("Dereferencing any_of is true if condition met by a single collection element.", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(-1), make_shared<int>(2), make_shared<int>(-3)};
	auto loop_count = 0;
	// Notice how that lambda gets passed in an integer reference, not a smart pointer. Even though
	// passing references to primitive types makes no sense. This is just for simplicity here.
	const auto is_positive_number = [&](const int &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = deref::any_of(some_numbers, is_positive_number);

	REQUIRE(actual == true);
	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "-12");
}

TEST_CASE("Dereferencing any_of is false if condition violated by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(-1), make_shared<int>(-2), make_shared<int>(-3)};
	auto loop_count = 0;
	// Notice how that lambda gets passed in an integer reference, not a smart pointer. Even though
	// passing references to primitive types makes no sense. This is just for simplicity here.
	const auto is_positive_number = [&](const int &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = deref::any_of(some_numbers, is_positive_number);

	REQUIRE(actual == false);
	REQUIRE(loop_count == 3);
	REQUIRE(result.str() == "-1-2-3");
}

TEST_CASE("Dereferencing none_of is true if condition violated by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(-1), make_shared<int>(-2), make_shared<int>(-3)};
	auto loop_count = 0;
	// Notice how that lambda gets passed in an integer reference, not a smart pointer. Even though
	// passing references to primitive types makes no sense. This is just for simplicity here.
	const auto is_positive_number = [&](const int &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = deref::none_of(some_numbers, is_positive_number);

	REQUIRE(actual == true);
	REQUIRE(loop_count == 3);
	REQUIRE(result.str() == "-1-2-3");
}

TEST_CASE("Dereferencing none_of is false if condition met by all collection elements.", "[UT, fast]")
{
	ostringstream result;
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(-1), make_shared<int>(2), make_shared<int>(-3)};
	auto loop_count = 0;
	// Notice how that lambda gets passed in an integer reference, not a smart pointer. Even though
	// passing references to primitive types makes no sense. This is just for simplicity here.
	const auto is_positive_number = [&](const int &n)
	{
		loop_count++;
		result << n;
		return n >= 0;
	};

	const auto actual = deref::none_of(some_numbers, is_positive_number);

	REQUIRE(actual == false);
	REQUIRE(loop_count == 2);
	REQUIRE(result.str() == "-12");
}

TEST_CASE("Dereferencing contains is true if collection contains given element.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};

	const auto actual = deref::contains(some_numbers, 2);

	REQUIRE(actual == true);
}

TEST_CASE("Dereferencing contains is false if collection does not contain given element.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};

	const auto actual = deref::contains(some_numbers, 4711);

	REQUIRE(actual == false);
}

TEST_CASE("A set contains itself.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};

	REQUIRE(deref::contains_all(some_numbers, some_numbers));
}

TEST_CASE("An empty set contains itself.", "[UT, fast]")
{
	const vector<shared_ptr<int>> empty_list;

	REQUIRE(deref::contains_all(empty_list, empty_list));
}

TEST_CASE("A nonempty set contains an empty set.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	const vector<shared_ptr<int>> empty_list;

	REQUIRE(deref::contains_all(some_numbers, empty_list));
}

TEST_CASE("An empty set does not contain a nonempty set.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	const vector<shared_ptr<int>> empty_list;

	REQUIRE_FALSE(deref::contains_all(empty_list, some_numbers));
}

TEST_CASE("{1,2,3} does not contain all {1,2,3,4,5}.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	const vector<shared_ptr<int>> some_more_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3),
	                                                make_shared<int>(4), make_shared<int>(5)};

	REQUIRE_FALSE(deref::contains_all(some_numbers, some_more_numbers));
}

TEST_CASE("{1,2,3,4,5} contain all {1,2,3}.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	const vector<shared_ptr<int>> some_more_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3),
	                                                make_shared<int>(4), make_shared<int>(5)};

	REQUIRE(deref::contains_all(some_more_numbers, some_numbers));
}

TEST_CASE("Order of element irrelevant for contains_all.", "[UT, fast]")
{
	const vector<shared_ptr<int>> some_numbers{make_shared<int>(2), make_shared<int>(1), make_shared<int>(3)};
	const vector<shared_ptr<int>> some_more_numbers{make_shared<int>(4), make_shared<int>(2), make_shared<int>(3),
	                                                make_shared<int>(1), make_shared<int>(5)};

	REQUIRE(deref::contains_all(some_numbers, some_numbers));
	REQUIRE_FALSE(deref::contains_all(some_numbers, some_more_numbers));
	REQUIRE(deref::contains_all(some_more_numbers, some_numbers));
	REQUIRE(deref::contains_all(some_more_numbers, some_more_numbers));
}
