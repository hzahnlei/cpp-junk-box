/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/collection_builder.hpp"
#include <catch2/catch_all.hpp>
#include <memory>
#include <string>

using std::make_unique;
using std::string;

using namespace junkbox;

TEST_CASE("vectors with one element can be created.", "[UT, fast]")
{
	const auto actual = collection::as_vector(123);
	REQUIRE(actual.size() == 1);
	REQUIRE(actual[0] == 123);
}

TEST_CASE("vectors with multiple elements can be created.", "[UT, fast]")
{
	const auto actual = collection::as_vector(123, 456, 789);
	REQUIRE(actual.size() == 3);
	REQUIRE(actual[0] == 123);
	REQUIRE(actual[1] == 456);
	REQUIRE(actual[2] == 789);
}

TEST_CASE("vectors can be created holding a single unique_ptr.", "[UT, fast]")
{
	const auto actual = collection::as_vector(make_unique<string>("abc"));
	REQUIRE(actual.size() == 1);
	REQUIRE(*actual[0] == "abc");
}

TEST_CASE("vectors can be created holding multiple unique_ptr.", "[UT, fast]")
{
	const auto actual = collection::as_vector(make_unique<string>("abc"), make_unique<string>("def"),
	                                          make_unique<string>("ghi"));
	REQUIRE(actual.size() == 3);
	REQUIRE(*actual[0] == "abc");
	REQUIRE(*actual[1] == "def");
	REQUIRE(*actual[2] == "ghi");
}
