/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/abstract_debug_printable.hpp"
#include <catch2/catch_all.hpp>
#include <sstream>

using std::ostream;
using std::ostringstream;
using std::string;

using namespace junkbox;

class My_Debug_Printable final : public Abstract_Debug_Printable
{
    public:
	auto print_debug_representation(ostream &out, const string &indentation) const -> ostream & override
	{
		return out << indentation << "This is my debug printable class!";
	}
};

TEST_CASE("Can debug-print to debug stream.", "[UT, fast]")
{
	ostringstream debug_stream;
	const My_Debug_Printable my_debug_printable;
	debug_stream << my_debug_printable;
	REQUIRE(debug_stream.str() == "This is my debug printable class!");
}

TEST_CASE("nullptr can debug-print to debug stream.", "[UT, fast]")
{
	ostringstream debug_stream;
	debug_stream << (My_Debug_Printable *)nullptr;
	REQUIRE(debug_stream.str() == "nullptr");
}

TEST_CASE("Can debug-print to std stream with indentation.", "[UT, fast]")
{
	ostringstream debug_stream;
	const My_Debug_Printable my_debug_printable;
	my_debug_printable.print_debug_representation(debug_stream, "....");
	REQUIRE(debug_stream.str() == "....This is my debug printable class!");
}
