/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/junkbox.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Library version.", "[UT, fast]")
{
	REQUIRE(junkbox::VERSION_MAJOR == 1U);
	REQUIRE(junkbox::VERSION_MINOR == 7U);
	REQUIRE(junkbox::VERSION_PATCH == 3U);
	REQUIRE(junkbox::VERSION_EXTENSION == "");
}
