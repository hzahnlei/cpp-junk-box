/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/text.hpp"
#include <catch2/catch_all.hpp>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

using std::less;
using std::map;
using std::ostringstream;
using std::string;
using std::vector;

using namespace junkbox;

TEST_CASE("Whitespace is recognized.", "[UT, fast]")
{
	REQUIRE(text::contains_whitespace(" "));
	REQUIRE(text::contains_whitespace(" Hello"));
	REQUIRE(text::contains_whitespace("Hello "));
	REQUIRE(text::contains_whitespace(" Hello "));
	REQUIRE(text::contains_whitespace("Hello World!"));
	REQUIRE(text::contains_whitespace(" Hello World! "));
	REQUIRE(text::contains_whitespace("Hello\t"));
	REQUIRE(text::contains_whitespace("Hello\n"));
}

TEST_CASE("Absense of whitespace is recognized.", "[UT, fast]")
{
	REQUIRE_FALSE(text::contains_whitespace(""));
	REQUIRE_FALSE(text::contains_whitespace("Hello"));
	REQUIRE_FALSE(text::contains_whitespace(">="));
}

TEST_CASE("Single escape: Nothing escaped if there is nothing to be escaped.", "[UT, fast]")
{
	REQUIRE(text::escaped('"', "Hello World") == "Hello World");
}

TEST_CASE("Single escape: Escaped if required.", "[UT, fast]")
{
	REQUIRE(text::escaped('"', "Hello \"Holger\"") == "Hello \\\"Holger\\\"");
	REQUIRE(text::escaped('"', "Hello Holger\\Nina") == "Hello Holger\\\\Nina");
}

TEST_CASE("Double quoting works, simple case.", "[UT, fast]")
{
	REQUIRE(text::double_quoted("Hello World") == "\"Hello World\"");
}

TEST_CASE("Double quoting works, even if input contains double quotes.", "[UT, fast]")
{
	REQUIRE(text::double_quoted("Hello \"Holger\"") == "\"Hello \\\"Holger\\\"\"");
	REQUIRE(text::double_quoted("Hello Holger\\Nina") == "\"Hello Holger\\\\Nina\"");
}

TEST_CASE("Result surrounded by single quotes, simple case.", "[UT, fast]")
{
	REQUIRE(text::single_quoted("Hello World") == "'Hello World'");
}

TEST_CASE("Result surrounded by single quotes. Quotes from input escaped.", "[UT, fast]")
{
	REQUIRE(text::single_quoted("Hello 'Holger'") == "'Hello \\'Holger\\''");
	REQUIRE(text::single_quoted("Hello Holger\\Nina") == "'Hello Holger\\\\Nina'");
}

TEST_CASE("Result surrounded by brackets, simple case.", "[UT, fast]")
{
	REQUIRE(text::bracketed("Hello World") == "[Hello World]");
}

TEST_CASE("Result surrounded by brackets. Brackents from input escaped.", "[UT, fast]")
{
	REQUIRE(text::bracketed("Hello [Holger]") == "[Hello \\[Holger\\]]");
	REQUIRE(text::bracketed("Hello Holger\\Nina") == "[Hello Holger\\\\Nina]");
}

TEST_CASE("Multi escape: Nothing escaped if there is nothing to be escaped.", "[UT, fast]")
{
	REQUIRE(text::escaped("\"", "Hello World") == "Hello World");
}

TEST_CASE("Multi escape: Escaped if required.", "[UT, fast]")
{
	REQUIRE(text::escaped("\"", "Hello \"Holger\"") == "Hello \\\"Holger\\\"");
	REQUIRE(text::escaped("(),", "(Hello, World!)") == "\\(Hello\\, World!\\)");
}

TEST_CASE("Prefix recognized if present.", "[UT, fast]")
{
	REQUIRE(text::starts_with("blablub", "bla"));
	REQUIRE(text::starts_with("bla blub", "bla"));
}

TEST_CASE("Absense of prefix recognized.", "[UT, fast]")
{
	REQUIRE_FALSE(text::starts_with("blablub", "blub"));
	REQUIRE_FALSE(text::starts_with("bla blub", "blub"));
	REQUIRE_FALSE(text::starts_with(" blablub", "bla"));
}

TEST_CASE("All letters in result are upper case.", "[UT, fast]")
{
	REQUIRE(text::upper_case("") == "");
	REQUIRE(text::upper_case("123") == "123");
	REQUIRE(text::upper_case("ABC") == "ABC");
	REQUIRE(text::upper_case("abc") == "ABC");
	REQUIRE(text::upper_case("AbC 123 xYz") == "ABC 123 XYZ");
}

TEST_CASE("Empty string is indented.", "[UT, fast, indented]")
{
	const auto actual = text::indented("--- ", "");
	REQUIRE(actual == "--- ");
}

TEST_CASE("Single line is indented.", "[UT, fast, indented]")
{
	const auto actual = text::indented("--- ", "abc");
	REQUIRE(actual == "--- abc");
}

TEST_CASE("Multiple lines are indented.", "[UT, fast, indented]")
{
	const auto actual = text::indented("--- ", "abc\n"
	                                           "xyz");
	REQUIRE(actual == "--- abc\n"
	                  "--- xyz");
}

TEST_CASE("Blank line is indented.", "[UT, fast, indented]")
{
	const auto actual = text::indented("--- ", "abc\n"
	                                           "\n"
	                                           "xyz");
	REQUIRE(actual == "--- abc\n"
	                  "--- \n"
	                  "--- xyz");
}

TEST_CASE("Nothing is put onto stream for empty map.", "[UT, fast, map]")
{
	ostringstream result;
	map<string, int> input;
	result << text::representation_of_keys(input);
	REQUIRE(result.str() == "");
}

TEST_CASE("Single key is put on stream for map with one entry.", "[UT, fast, map]")
{
	ostringstream result;
	map<string, int> input{{"one", 1}};
	result << text::representation_of_keys(input);
	REQUIRE(result.str() == "'one'");
}

TEST_CASE("Three keys are put on stream for map with three entries.", "[UT, fast, map]")
{
	ostringstream result;
	const map<string, int> input{{"one", 1}, {"two", 2}, {"three", 3}};
	result << text::representation_of_keys(input);
	REQUIRE(result.str() == "'one', 'three', 'two'");
}

TEST_CASE("Textual representation of keys does not have to be quoted.", "[UT, fast, map]")
{
	ostringstream result;
	const map<string, int> input{{"one", 1}, {"two", 2}, {"three", 3}};
	result << text::representation_of_keys(input, text::do_not_quote_elements);
	REQUIRE(result.str() == "one, three, two");
}

TEST_CASE("Can use any transform on keys.", "[UT, fast, map]")
{
	ostringstream result;
	const map<string, int> input{{"one", 1}, {"two", 2}, {"three", 3}};
	result << text::representation_of_keys(input, text::upper_case);
	REQUIRE(result.str() == "ONE, THREE, TWO");
}

TEST_CASE("Can put textual representation of all map entrys on stream.", "[UT, fast, map]")
{
	ostringstream result;
	const map<string, int> input{{"one", 1}, {"two", 2}, {"three", 3}};
	result << text::representation_of_key_value_pairs(input);
	REQUIRE(result.str() == "'one' -> 1, 'three' -> 3, 'two' -> 2");
}

TEST_CASE("Can put textual representation of all map entrys on stream with keys transformed.", "[UT, fast, map]")
{
	ostringstream result;
	const map<string, int> input{{"one", 1}, {"two", 2}, {"three", 3}};
	result << text::representation_of_key_value_pairs(input, text::upper_case);
	REQUIRE(result.str() == "ONE -> 1, THREE -> 3, TWO -> 2");
}

TEST_CASE("Nothing is put onto stream for empty list.", "[UT, fast, list]")
{
	ostringstream result;
	const vector<string> input;
	result << text::representation_of_elements(input);
	REQUIRE(result.str() == "");
}

TEST_CASE("Single element is put on stream for list with one entry.", "[UT, fast, list]")
{
	ostringstream result;
	const vector<string> input{"one"};
	result << text::representation_of_elements(input);
	REQUIRE(result.str() == "'one'");
}
// WIP create separate tests for comma separation, ordering etc.
TEST_CASE("Three elements are put on stream for list with three entries.", "[UT, fast, list]")
{
	ostringstream result;
	const vector<string> input{"one", "two", "three"};
	result << text::representation_of_elements(input);
	REQUIRE(result.str() == "'one', 'two', 'three'");
}

TEST_CASE("Textual representation of list elements does not have to be quoted.", "[UT, fast, list]")
{
	ostringstream result;
	const vector<string> input{"one", "two", "three"};
	result << text::representation_of_elements(input, text::do_not_quote_elements);
	REQUIRE(result.str() == "one, two, three");
}

TEST_CASE("Can use any transform on list elements.", "[UT, fast, list]")
{
	ostringstream result;
	const vector<string> input{"one", "two", "three"};
	result << text::representation_of_elements(input, text::upper_case);
	REQUIRE(result.str() == "ONE, TWO, THREE");
}
