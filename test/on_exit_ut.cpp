/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/on_exit.hpp"
#include <catch2/catch_all.hpp>
#include <sstream>
#include <stdexcept>

using std::ostringstream;
using std::runtime_error;

using namespace junkbox;

auto function_that_exits_normally(ostringstream &out)
{
	On_Exit _{[&]() { out << "Exit action invoked."; }};
	out << "Do something. ";
}

TEST_CASE("Exit-action is invoked at the end of function that exits normally.", "[UT, fast]")
{
	ostringstream out;
	function_that_exits_normally(out);
	REQUIRE(out.str() == "Do something. Exit action invoked.");
}

auto function_that_exists_with_exception(ostringstream &out)
{
	On_Exit _{[&]() { out << "Exit action invoked."; }};
	out << "Do something. ";
	throw runtime_error{"Crash!"};
}

TEST_CASE("Exit-action is invoked at the end of function that exits abnormally.", "[UT, fast]")
{
	ostringstream out;
	REQUIRE_THROWS_MATCHES(function_that_exists_with_exception(out), runtime_error,
	                       Catch::Matchers::Message("Crash!"));
	REQUIRE(out.str() == "Do something. Exit action invoked.");
}

auto function_that_exits_normally_using_macro(ostringstream &out)
{
	// You can use this macro for convenience. But note that there can only
	// one such macro per block.
	ON_EXIT_DO(out << "Exit action invoked.");
	out << "Do something. ";
}

TEST_CASE("Can use a convenience macro.", "[UT, fast]")
{
	ostringstream out;
	function_that_exits_normally_using_macro(out);
	REQUIRE(out.str() == "Do something. Exit action invoked.");
}

auto function_with_multiple_actions_that_exists_with_exception(ostringstream &out)
{
	On_Exit _1{[&]() { out << "Exit action1 invoked."; }};
	out << "Do something. ";
	On_Exit _2{[&]() { out << "Exit action2 invoked."; }};
	On_Exit _3{[&]() { out << "Exit action3 invoked."; }};
	throw runtime_error{"Crash!"};
}

TEST_CASE("Can have multiple on-exit actions.", "[UT, fast]")
{
	ostringstream out;
	REQUIRE_THROWS_MATCHES(function_with_multiple_actions_that_exists_with_exception(out), runtime_error,
	                       Catch::Matchers::Message("Crash!"));
	REQUIRE(out.str() == "Do something. Exit action3 invoked.Exit action2 invoked.Exit action1 invoked.");
}
