/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <cstddef>
#include <functional>
#include <string>

namespace test
{

	using std::function;
	using std::size_t;
	using std::string;

	class Files_And_Dirs_Test_Fixture
	{
	    private:
		string m_directory_name;
		string m_sub_directory_name;
		using Dir_Predicate_Fun = function<bool(const string &)>;
		bool m_actual_bool = false;
		Dir_Predicate_Fun m_check;
		using File_Count_Fun = function<size_t(const string &)>;
		File_Count_Fun m_count;
		size_t m_actual_count = 0;
		using Delete_Folder_Fun = function<void(const string &)>;
		Delete_Folder_Fun m_delete;
		string m_actual_file_content;
		using Load_File_Fun = function<string(const string &)>;
		Load_File_Fun m_load;
		string m_cause;

	    public:
		Files_And_Dirs_Test_Fixture() = default;
		virtual ~Files_And_Dirs_Test_Fixture() = default;

		auto given_empty_directory(const string &) -> void;
		auto and_a_file_is_stored_in_that_directory(const string &file_name,
		                                            const string &content = "This file can be deleted.") const
				-> void;

		auto given_empty_sub_directory(const string &) -> void;
		auto
		and_a_file_is_stored_in_that_sub_directory(const string &file_name,
		                                           const string &content = "This file can be deleted.") const
				-> void;

		auto when_we_apply(const Dir_Predicate_Fun &) -> Files_And_Dirs_Test_Fixture &;
		auto on_directory(const string &) const -> bool;
		auto then_we_expect(const bool) -> Files_And_Dirs_Test_Fixture &;
		auto to_be(const bool) const -> void;

		auto when_we_run(const File_Count_Fun &) -> Files_And_Dirs_Test_Fixture &;
		auto on_given_directory(const string &) const -> size_t;
		auto then_we_expect(const size_t) -> Files_And_Dirs_Test_Fixture &;
		auto to_be(const size_t) const -> void;
		auto on_given_nonexistent_directory(const string &) -> void;

		auto when_we_call(const Delete_Folder_Fun &) -> Files_And_Dirs_Test_Fixture &;
		auto on_that_directory() const -> void;
		auto then_that_directory_should_still_exist() const -> void;
		auto but_all_files_in_that_directory_should_be_gone() const -> void;
		auto but_all_files_in_that_parent_directory_should_be_gone() const -> void;
		auto and_that_sub_directory_should_still_exist() const -> void;
		auto but_all_files_in_that_sub_directory_should_be_gone() const -> void;

		auto on_nonexisting_directory(const string &) -> void;
		auto then_this_should_succeed() -> Files_And_Dirs_Test_Fixture &;
		auto without_any_effect() const -> void;

		auto when_we_invoke(const Load_File_Fun &) -> Files_And_Dirs_Test_Fixture &;
		auto on_file(const string &) -> string;
		auto then_we_expect(const string &) -> Files_And_Dirs_Test_Fixture &;
		auto to_be(const string &) const -> void;

		auto on_nonexisting_file(const string &) -> void;
		auto then_this_should_fail() const -> void;
		auto and_we_expect_cause() -> Files_And_Dirs_Test_Fixture &;
	};

} // namespace test
