/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "files_and_dirs_test_fixture.hpp"
#include "junkbox/files_and_dirs.hpp"
#include "junkbox/text.hpp"
#include <catch2/catch_all.hpp>
#include <fstream>
#include <sys/stat.h>

namespace test
{

	using std::exception;
	using std::ofstream;

	using namespace junkbox;

	auto create_empty_dir(const string &dir_name)
	{
		if (!file::directory_exists(dir_name))
		{
			if (0 != mkdir(dir_name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))
			{
				throw junkbox::file::File_Error{"Failed to create directory " +
				                                text::single_quoted(dir_name) + "."};
			}
		}
		else
		{
			file::delete_all_content_from_directory(dir_name);
		}
	}

	auto Files_And_Dirs_Test_Fixture::given_empty_directory(const string &dir_name) -> void
	{
		m_directory_name = dir_name;
		create_empty_dir(m_directory_name);
	}

	auto Files_And_Dirs_Test_Fixture::and_a_file_is_stored_in_that_directory(const string &file_name,
	                                                                         const string &content) const -> void
	{
		ofstream file{m_directory_name + '/' + file_name};
		file << content;
	}

	auto Files_And_Dirs_Test_Fixture::given_empty_sub_directory(const string &dir_name) -> void
	{
		m_sub_directory_name = m_directory_name + '/' + dir_name;
		create_empty_dir(m_sub_directory_name);
	}

	auto Files_And_Dirs_Test_Fixture::and_a_file_is_stored_in_that_sub_directory(const string &file_name,
	                                                                             const string &content) const
			-> void
	{
		ofstream file{m_sub_directory_name + '/' + file_name};
		file << content;
	}

	auto Files_And_Dirs_Test_Fixture::when_we_apply(const Dir_Predicate_Fun &check) -> Files_And_Dirs_Test_Fixture &
	{
		m_check = check;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::on_directory(const string &dir_name) const -> bool
	{
		return m_check(dir_name);
	}

	auto Files_And_Dirs_Test_Fixture::then_we_expect(const bool actual) -> Files_And_Dirs_Test_Fixture &
	{
		m_actual_bool = actual;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::to_be(const bool expected) const -> void
	{
		REQUIRE(m_actual_bool == expected);
	}

	auto Files_And_Dirs_Test_Fixture::when_we_run(const File_Count_Fun &count) -> Files_And_Dirs_Test_Fixture &
	{
		m_count = count;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::on_given_directory(const string &dir_name) const -> size_t
	{
		return m_count(dir_name);
	}

	auto Files_And_Dirs_Test_Fixture::then_we_expect(const size_t actual) -> Files_And_Dirs_Test_Fixture &
	{
		m_actual_count = actual;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::to_be(const size_t expected) const -> void
	{
		REQUIRE(m_actual_count == expected);
	}

	auto Files_And_Dirs_Test_Fixture::on_given_nonexistent_directory(const string &dir_name) -> void
	{
		try
		{
			m_count(dir_name);
			m_cause = "";
		}
		catch (const junkbox::file::File_Error &cause)
		{
			m_cause = cause.what();
		}
	}

	auto Files_And_Dirs_Test_Fixture::when_we_call(const Delete_Folder_Fun &delete_fun)
			-> Files_And_Dirs_Test_Fixture &
	{
		m_delete = delete_fun;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::on_that_directory() const -> void { m_delete(m_directory_name); }

	auto Files_And_Dirs_Test_Fixture::then_that_directory_should_still_exist() const -> void
	{
		REQUIRE(file::directory_exists(m_directory_name));
	}

	auto Files_And_Dirs_Test_Fixture::but_all_files_in_that_directory_should_be_gone() const -> void
	{
		REQUIRE(file::file_count(m_directory_name) == 2); // . and .. always there
	}

	auto Files_And_Dirs_Test_Fixture::but_all_files_in_that_parent_directory_should_be_gone() const -> void
	{
		REQUIRE(file::file_count(m_directory_name) == 3); // . and .. always there plus the sub-directory we
		                                                  // created
	}

	auto Files_And_Dirs_Test_Fixture::and_that_sub_directory_should_still_exist() const -> void
	{
		REQUIRE(file::directory_exists(m_sub_directory_name));
	}

	auto Files_And_Dirs_Test_Fixture::but_all_files_in_that_sub_directory_should_be_gone() const -> void
	{
		REQUIRE(file::file_count(m_sub_directory_name) == 2); // . and .. always there
	}

	auto Files_And_Dirs_Test_Fixture::on_nonexisting_directory(const string &directory_name) -> void
	{
		try
		{
			m_delete(directory_name);
			m_cause = "";
		}
		catch (const junkbox::file::File_Error &cause)
		{
			m_cause = cause.what();
		}
	}

	auto Files_And_Dirs_Test_Fixture::then_this_should_succeed() -> Files_And_Dirs_Test_Fixture &
	{
		REQUIRE(m_cause == "");
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::without_any_effect() const -> void
	{
		REQUIRE(file::directory_exists(m_directory_name));
		REQUIRE(file::file_count(m_directory_name) == 2);
	}

	auto Files_And_Dirs_Test_Fixture::when_we_invoke(const Load_File_Fun &load_fun) -> Files_And_Dirs_Test_Fixture &
	{
		m_load = load_fun;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::on_file(const string &file_name) -> string { return m_load(file_name); }

	auto Files_And_Dirs_Test_Fixture::then_we_expect(const string &actual_file_content)
			-> Files_And_Dirs_Test_Fixture &
	{
		m_actual_file_content = actual_file_content;
		return *this;
	}

	auto Files_And_Dirs_Test_Fixture::to_be(const string &expected_content) const -> void
	{
		REQUIRE(m_actual_file_content == expected_content);
	}

	auto Files_And_Dirs_Test_Fixture::on_nonexisting_file(const string &file_name) -> void
	{
		try
		{
			m_load(file_name);
			m_cause = "";
		}
		catch (const junkbox::file::File_Error &cause)
		{
			m_cause = cause.what();
		}
	}

	auto Files_And_Dirs_Test_Fixture::then_this_should_fail() const -> void { REQUIRE(m_cause != ""); }

	auto Files_And_Dirs_Test_Fixture::and_we_expect_cause() -> Files_And_Dirs_Test_Fixture &
	{
		m_actual_file_content = m_cause;
		return *this;
	}

} // namespace test
