/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/collection_builder.hpp"
#include "junkbox/deref_algorithms.hpp"
#include "junkbox/text.hpp"
#include <catch2/catch_all.hpp>
#include <memory>
#include <string>
#include <type_traits>

using std::is_const;
using std::make_unique;
using std::string;
using std::unique_ptr;
using std::vector;

using namespace std::literals::string_literals;
using namespace junkbox;

TEST_CASE("Code that operates on collections of unique_ptrs can be tested more "
          "conveniently using collection builder.",
          "[CT, fast]")
{
	const auto some_collection = collection::as_vector(make_unique<string>("a"), make_unique<string>("b"),
	                                                   make_unique<string>("c"));
	REQUIRE(deref::contains(some_collection, "a"s));
	REQUIRE_FALSE(deref::contains(some_collection, "z"s));
}

TEST_CASE("The convenienly built and the discretely built collections are "
          "equivalent. But the first is shorter to "
          "write.",
          "[CT, fast]")
{
	const auto some_collection = collection::as_vector(make_unique<string>("a"), make_unique<string>("b"),
	                                                   make_unique<string>("c"));

	vector<unique_ptr<string>> some_other_collection;
	some_other_collection.emplace_back(make_unique<string>("a"));
	some_other_collection.emplace_back(make_unique<string>("b"));
	some_other_collection.emplace_back(make_unique<string>("c"));

	REQUIRE(deref::contains_all(some_collection, some_other_collection));
	REQUIRE(deref::contains_all(some_other_collection, some_collection));
}

TEST_CASE("The convenienly built collection is const the discretely built "
          "collection is mutable.",
          "[CT, fast]")
{
	const auto some_collection = collection::as_vector(make_unique<string>("a"), make_unique<string>("b"),
	                                                   make_unique<string>("c"));

	vector<unique_ptr<string>> some_other_collection;
	some_other_collection.emplace_back(make_unique<string>("a"));
	some_other_collection.emplace_back(make_unique<string>("b"));
	some_other_collection.emplace_back(make_unique<string>("c"));

	REQUIRE(is_const<decltype(some_collection)>::value);
	REQUIRE_FALSE(is_const<decltype(some_other_collection)>::value);
}
