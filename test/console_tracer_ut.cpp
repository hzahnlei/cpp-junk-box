/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/console_tracer.hpp"
#include <catch2/catch_all.hpp>
#include <sstream>
#include <stdexcept>

using Catch::Matchers::Message;
using std::ostringstream;
using std::runtime_error;

using namespace junkbox;

auto function_that_traces(ostream &out) { TRACE(out); }

TEST_CASE("Function traces entry and exit upon normal termination.", "[UT, fast]")
{
	ostringstream out;
	function_that_traces(out);
#if defined(__clang__) && (__clang_major__ < 13)
	REQUIRE(out.str() == "ENTER auto function_that_traces(std::__1::ostream &)\n"
	                     "EXIT auto function_that_traces(std::__1::ostream &)\n");
#elif defined(__clang__) && (__clang_major__ >= 13)
	REQUIRE(out.str() == "ENTER auto function_that_traces(std::ostream &)\n"
	                     "EXIT auto function_that_traces(std::ostream &)\n");
#else
	REQUIRE(out.str() == "ENTER auto function_that_traces(std::ostream&)\n"
	                     "EXIT auto function_that_traces(std::ostream&)\n");
#endif
}

TEST_CASE("Lambda traces entry and exit upon normal termination.", "[UT, fast]")
{
	ostringstream out;
	[&]() { TRACE(out); }();
#if defined(__clang__)
	REQUIRE(out.str() == "ENTER auto CATCH2_INTERNAL_TEST_2()::(anonymous class)::operator()() const\n"
	                     "EXIT auto CATCH2_INTERNAL_TEST_2()::(anonymous class)::operator()() const\n");
#else
	REQUIRE(out.str() == "ENTER CATCH2_INTERNAL_TEST_2()::<lambda()>\n"
	                     "EXIT CATCH2_INTERNAL_TEST_2()::<lambda()>\n");
#endif
}

[[noreturn]] auto function_that_traces_and_throws(ostream &out)
{
	TRACE(out);
	throw runtime_error{"Ups!"};
}

TEST_CASE("Function traces entry and exit upon abnormal termination.", "[UT, fast]")
{
	ostringstream out;
	REQUIRE_THROWS_MATCHES(function_that_traces_and_throws(out), runtime_error, Message("Ups!"));
#if defined(__clang__) && (__clang_major__ < 13)
	REQUIRE(out.str() == "ENTER auto function_that_traces_and_throws(std::__1::ostream &)\n"
	                     "ABORT auto function_that_traces_and_throws(std::__1::ostream &)\n");
#elif defined(__clang__) && (__clang_major__ >= 13)
	REQUIRE(out.str() == "ENTER auto function_that_traces_and_throws(std::ostream &)\n"
	                     "ABORT auto function_that_traces_and_throws(std::ostream &)\n");
#else
	REQUIRE(out.str() == "ENTER auto function_that_traces_and_throws(std::ostream&)\n"
	                     "ABORT auto function_that_traces_and_throws(std::ostream&)\n");
#endif
}

TEST_CASE("Lambda traces entry and exit upon abnormal termination.", "[UT, fast]")
{
	ostringstream out;
	const auto lambda_that_traces_and_throws = [&]()
	{
		TRACE(out);
		throw runtime_error{"Ups!"};
	};
	REQUIRE_THROWS_MATCHES(lambda_that_traces_and_throws(), runtime_error, Message("Ups!"));
#if defined(__clang__)
	REQUIRE(out.str() == "ENTER auto CATCH2_INTERNAL_TEST_6()::(anonymous class)::operator()() const\n"
	                     "ABORT auto CATCH2_INTERNAL_TEST_6()::(anonymous class)::operator()() const\n");
#else
	REQUIRE(out.str() == "ENTER CATCH2_INTERNAL_TEST_6()::<lambda()>\n"
	                     "ABORT CATCH2_INTERNAL_TEST_6()::<lambda()>\n");
#endif
}
