/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/crtp.hpp"
#include <catch2/catch_all.hpp>
#include <sstream>

using std::istringstream;
using std::string;

using namespace junkbox;

template <class P> class Parser_For_Ints : public Crtp<P, Parser_For_Ints>
{
    public:
	auto parse_int() -> string
	{
		string result{"(INT "};
		while (isdigit(this->underlying().m_input.peek()))
		{
			result += static_cast<char>(this->underlying().m_input.get());
		}
		return result + ") ";
	}
};

template <class P> class Parser_For_Operators : public Crtp<P, Parser_For_Operators>
{
    public:
	auto parse_operator() -> string
	{
		string result{"(OP "};
		result += static_cast<char>(this->underlying().m_input.get());
		return result + ") ";
	}
};

class Parser final : public Parser_For_Ints<Parser>, public Parser_For_Operators<Parser>
{
	friend Parser_For_Ints; // Make friends so that friend can use my
	                        // protected members.
	friend Parser_For_Operators;

	istringstream m_input;

    public:
	explicit Parser(const string &text) : m_input{text} {}
	auto parse() -> string
	{
		string result;
		while (!m_input.eof())
		{
			if (isdigit(m_input.peek()))
			{
				result += parse_int();
			}
			else
			{
				result += parse_operator();
			}
		}
		return result;
	}
};

TEST_CASE("A parser can be put together from different parts by CRTP.", "[UT, fast]")
{
	Parser parser{"1+2"};
	const auto actual = parser.parse();
	REQUIRE(actual == "(INT 1) (OP +) (INT 2) ");
}
