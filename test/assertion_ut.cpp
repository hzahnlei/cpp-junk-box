/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/assertion.hpp"
#include <catch2/catch_all.hpp>
#include <memory>
#include <sstream>
#include <string>

using Catch::Matchers::EndsWith;
using Catch::Matchers::StartsWith;
using std::make_shared;
using std::ostringstream;
using std::string;

using namespace junkbox;

TEST_CASE("Name of file were assertion was violated can be retrieved.", "[UT, fast]")
{
	Assertion_Violation violation{"some file name", 13, "some message"};
	REQUIRE(violation.file() == "some file name");
}

TEST_CASE("Line number were assertion was violated can be retrieved.", "[UT, fast]")
{
	Assertion_Violation violation{"some file name", 13, "some message"};
	REQUIRE(violation.line() == 13);
}

TEST_CASE("Message explaining the assertion violation can be retrieved.", "[UT, fast]")
{
	Assertion_Violation violation{"some file name", 13, "some message"};
	REQUIRE(violation.message() == "some message");
	REQUIRE(string{violation.what()} == "some message");
	REQUIRE(violation.pretty_message() == "Assertion violated in some file name at 13: some message");
}

TEST_CASE("Assertion violation can 'printed' on a stream'.", "[UT, fast]")
{
	Assertion_Violation violation{"some file name", 13, "some message"};
	ostringstream result;
	result << violation;
	REQUIRE(result.str() == "Assertion violated in some file name at 13: some message");
}

TEST_CASE("Even pointers to assertion violation can 'printed' on a stream'.", "[UT, fast]")
{
	const auto violation = make_shared<Assertion_Violation>("some file name", 666, "some message");
	ostringstream result;
	result << violation;
	REQUIRE(result.str() == "Assertion violated in some file name at 666: some message");
}

TEST_CASE("No exception if condition holds.", "[UT, fast]")
{
	const int *p = nullptr;
	DEV_ASSERT(p == nullptr, "p must be nullptr.");
	SUCCEED();
}

TEST_CASE("Exception if condition does not hold.", "[UT, fast]")
{
	try
	{
		const int *p = nullptr;
		DEV_ASSERT(p != nullptr, "p must not be nullptr.");
		FAIL("Expected exception.");
	}
	catch (const Assertion_Violation &cause)
	{
		REQUIRE(cause.message() == "p must not be nullptr.");
		REQUIRE(string{cause.what()} == "p must not be nullptr.");
		REQUIRE_THAT(cause.pretty_message(), StartsWith("Assertion violated in"));
		REQUIRE_THAT(cause.pretty_message(), EndsWith("assertion_ut.cpp at 69: p must not be nullptr."));
	}
}
