/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include <catch2/catch_all.hpp>
#include <junkbox/junkbox.hpp>

/**
 * @brief A view random samples to see whether the library can be included and linked.
 */
TEST_CASE("Verify package version", "[package-test]")
{
	REQUIRE(junkbox::VERSION_MAJOR == 1);
	REQUIRE(junkbox::VERSION_MINOR == 7);
	REQUIRE(junkbox::VERSION_PATCH == 3);
	REQUIRE(junkbox::VERSION_EXTENSION == "");
}

TEST_CASE("Verify text functions", "[package-test]")
{
	REQUIRE(junkbox::text::single_quoted("Hello, World!") == "'Hello, World!'");
	REQUIRE(junkbox::text::double_quoted("Hello, World!") == "\"Hello, World!\"");
}
