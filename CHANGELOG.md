# Changelog

## 1.7.3 (2023-10-24)

*  Linter findings fixed
   *  Some findings intentionally ignored
   *  Some cannot be fixed because not all compilers fully support all C++ 20 features
*  Package tests now using Catch2 to perform a view random sample test to make sure
   *  headers from lib can be included by comsumer
   *  consumer can be linked against lib

## 1.7.2 (2023-10-21)

*  `.gitignore` the `documentation` folder.

## 1.7.1 (2023-10-21)

* `REDME.md` reworked.

## 1.7.0 (2023-10-20)

* Upgraded to Conan 2.0.

## 1.6.3 (2023-05-06)

*  Resoved an issue where `overloaded_rec`s that are `const` did not handle the default case (`auto`) correctly for non-exhaustive overloads
*  Example for use of `overloaded` vs. `overloaded_rec`

## 1.6.2 (2023-04-30)

* Small fixes (typos, etc.)

## 1.6.1 (2023-04-18)

* Cleaning up CMake files

## 1.6.0 (2023-04-16)

*  Added support for recursive structures when using `std::variant` and `std::visit`
*  Added API documentation (Doxygen)
*  Use newer build image

## 1.5.4 (2023-03-03)

*  Upgrading Catch2 from 2.13.8 to 3.3.2
   *  No need for `main.cpp` anymore, use Catch's `main`

## 1.5.3 (2022-04-18)

*  Use `[[nodiscard]]` attribute when appropriate
*  Make sure to use initializer lists
*  Fix SonarLint findings

## 1.5.2 (2022-04-17)

*  Fix SonarLint findings

## 1.5.1 (2022-04-16)

*  Simplifying test tags
*  Do not track history of `.vscode` anymore
*  Build image used does not reference deprecated Conan registry on Bintray anymore

## 1.5.0 (2022-04-15)

Using C++20

## 1.4.1 (2020-12-24)

Small code simplifications.

## 1.4.0 (2020-12-23)

Added virtual class `Abstract_Debug_Printable`.
It is similar like Java's `toString()` method, that is present in all classes.
Having a `<<` operator defined on important classes can help developing/debugging your code.
For example, Catch 2 uses it to produce readable error messages.

## 1.3.3 (2020-04-06)

Added missing unit tests for text:indented.

## 1.3.2 (2020-04-04)

Now including `<stdexpect>` instead of `<exception>`.
Including `<exception>` did not make any trouble until I used this lib in one of my projects.
Worked fine with Clang but failed with GCC.

## 1.3.1 (2020-04-04)

Fixed a bug that prevented `RUNTIME_ERROR` macro to be invoked with komplex expression. Only simple strings worked correctly.

## 1.3.0 (2020-04-04)

Added `RUNTIME_ERROR` macro as a convenient way of throwing a runtime exception including file name and line number where exception occurred.

## 1.2.0 (2020-04-01)

*  Added algorithms that iterate a collection of (smart) pointers and dereference the elements before calling predicate functions etc.
*  Added functions for creating textual representations of collections (writing to an `ostream`).
*  Message returned by `base_exception::message` is now `const&`.

## 1.1.3 (2020-03-03)

*  Fixing issues found by Clang static analyzer.
*  Simplifying test cases by using initializer lists instead of many lines of `emplace_back`.
*  Adding documentation.

## 1.1.2 (2020-02-11)

*  Changed name of uploaded test report to be more specific.
*  Harmonized required CMake versions between different `CMakeLists.txt` files.
*  Regex for determining coverage now configured (and therefore documented) in CI file.
   Before, it was not so explicitly configured on the CI admin web site.

## 1.1.1 (2020-02-10)

Fixed typos in documentation.

## 1.1.0 (2020-02-09)

*  Additional test cases.
*  Tiny improvements.

## 1.0.0 (2020-02-09)

*  Initial project set-up.
*  First library functions and classes.
*  Automated CI build.
*  Uses Conan package manager.
