/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/os_error.hpp"
#include <errno.h>
#include <sstream>
#include <string.h>

namespace junkbox
{

	using std::ostringstream;

	auto os_error_message(const User_Message &user_msg) -> OS_Message
	{
		ostringstream result;
		result << "OS error code #" << errno << " (" << (0 == errno ? "Success" : strerror(errno))
		       << "): " << user_msg;
		return result.str();
	}

} // namespace junkbox
