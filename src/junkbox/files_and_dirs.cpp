/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/files_and_dirs.hpp"
#include "junkbox/os_error.hpp"
#include "junkbox/text.hpp"
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <sys/errno.h>
#include <sys/stat.h>

namespace junkbox::file
{

	// Herein I am using some syscalls. Expect some rather C style
	// programming.

	using std::ifstream;

	/*
	 * RAII wrapper for safe use of opendir.
	 */
	class Auto_Closing_Dir final
	{
	    private:
		DIR *m_dir = nullptr;

	    public:
		explicit Auto_Closing_Dir(const Directory_Name &directory) : m_dir{opendir(directory.c_str())} {}
		explicit Auto_Closing_Dir(const Auto_Closing_Dir &) = delete;
		explicit Auto_Closing_Dir(Auto_Closing_Dir &&other) noexcept : m_dir{other.m_dir}
		{
			other.m_dir = nullptr;
		}
		~Auto_Closing_Dir()
		{
			if (nullptr != m_dir)
			{
				closedir(m_dir);
			}
		}

		auto operator=(const Auto_Closing_Dir &) -> Auto_Closing_Dir & = delete;
		auto operator=(Auto_Closing_Dir &&other) noexcept -> Auto_Closing_Dir & = delete;

		[[nodiscard]] explicit operator DIR *() const { return m_dir; }
	};

	[[nodiscard]] inline auto exists(const Auto_Closing_Dir &dir) { return nullptr != (DIR *)dir; }

	[[nodiscard]] inline auto exists(const struct dirent *dir_entry) { return nullptr != dir_entry; }

	[[nodiscard]] inline auto is_current_dir(const struct dirent &dir_entry)
	{
		return 0 == strcmp(".", dir_entry.d_name);
	}

	[[nodiscard]] inline auto is_parent_dir(const struct dirent &dir_entry)
	{
		return 0 == strcmp("..", dir_entry.d_name);
	}

	[[nodiscard]] inline auto is_directory(const struct dirent &dir_entry) { return DT_DIR == dir_entry.d_type; }

	[[nodiscard]] inline auto path_name(const Directory_Name &dir_name, const struct dirent &dir_entry)
	{
		return dir_name + '/' + dir_entry.d_name;
	}

	[[nodiscard]] inline auto delete_file(const File_Name &file_name)
	{
		if (0 != remove(file_name.c_str()))
		{
			throw File_Error{os_error_message("Failed to delete file " + text::single_quoted(file_name) +
			                                  " from directory.")};
		}
	}

	// Forward declaration.
	auto walk_directory_and_delete(const Directory_Name &dir_name, const bool also_delete_directories) -> void;

	auto delete_entry(const Directory_Name &dir_name, const struct dirent &dir_entry,
	                  const bool also_delete_directories)
	{
		if (is_directory(dir_entry))
		{
			if (!is_current_dir(dir_entry) && !is_parent_dir(dir_entry))
			{
				walk_directory_and_delete(path_name(dir_name, dir_entry), also_delete_directories);
				if (also_delete_directories)
				{
					delete_file(path_name(dir_name, dir_entry));
				}
			}
		}
		else
		{
			delete_file(path_name(dir_name, dir_entry));
		}
	}

	auto walk_directory_and_delete(const Directory_Name &dir_name, const bool also_delete_directories) -> void
	{
		Auto_Closing_Dir dir_to_be_cleaned{dir_name};
		if (exists(dir_to_be_cleaned))
		{
			auto dir_entry = readdir((DIR *)dir_to_be_cleaned);
			while (exists(dir_entry))
			{
				delete_entry(dir_name, *dir_entry, also_delete_directories);
				dir_entry = readdir((DIR *)dir_to_be_cleaned);
			}
		}
	}

	auto delete_files_from_directory(const Directory_Name &dir_name) -> void
	{
		walk_directory_and_delete(dir_name, false);
	}

	auto delete_all_content_from_directory(const Directory_Name &dir_name) -> void
	{
		walk_directory_and_delete(dir_name, true);
	}

	auto load_text_file(const File_Name &file_name) -> string
	{
		ifstream file{file_name};
		if (file.is_open())
		{
			string accumulated_content;
			string curr_line;
			auto i = 0U;
			while (getline(file, curr_line))
			{
				if (i > 0)
				{
					accumulated_content += '\n';
				}
				accumulated_content += curr_line;
				i++;
			}
			return accumulated_content;
		}
		else
		{
			throw File_Error{os_error_message("Failed to open file " + text::single_quoted(file_name) +
			                                  ".")};
		}
	}

	auto directory_exists(const Directory_Name &dir_name) -> bool
	{
		struct stat info;
		return (0 == stat(dir_name.c_str(), &info)) ? info.st_mode & S_IFDIR : false;
	}

	auto file_count(const Directory_Name &dir_name) -> size_t
	{
		Auto_Closing_Dir dir{dir_name};
		if (!(DIR *)dir)
		{
			throw File_Error{os_error_message("Failed to count files in directory " +
			                                  text::single_quoted(dir_name) + ".")};
		}
		auto count = 0U;
		while (readdir((DIR *)dir))
		{
			++count; // Also counts . and ..
		}
		return count;
	}

} // namespace junkbox::file
