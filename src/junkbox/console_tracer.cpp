/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/console_tracer.hpp"
#include <exception>

namespace junkbox
{

	using std::uncaught_exceptions;

	Console_Tracer::Console_Tracer(ostream &out, const char *const pretty_function)
			: m_out{out}, m_pretty_function{pretty_function}
	{
		m_out << "ENTER " << pretty_function << '\n';
	}

	Console_Tracer::~Console_Tracer()
	{
		if (uncaught_exceptions())
		{
			m_out << "ABORT " << m_pretty_function << '\n';
		}
		else
		{
			m_out << "EXIT " << m_pretty_function << '\n';
		}
	}

} // namespace junkbox
