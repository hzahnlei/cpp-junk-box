/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/assertion.hpp"

namespace junkbox
{

	using std::to_string;

	Assertion_Violation::Assertion_Violation(const File_Name &file, const Line_Number line, const Message &message)
			: Base_Exception{message}, m_file{file}, m_line{line}
	{
	}

	auto Assertion_Violation::pretty_message() const noexcept -> Message
	{
		try
		{
			// Replace with std::format as soon as compiler supports this C++ 20 feature.
			return "Assertion violated in " + m_file + " at " + to_string(m_line) + ": " + message();
		}
		catch (...)
		{
			return "";
		}
	}

	auto Assertion_Violation::file() const noexcept -> const File_Name & { return m_file; }

	auto Assertion_Violation::line() const noexcept -> Line_Number { return m_line; }

} // namespace junkbox
