/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/abstract_debug_printable.hpp"

namespace junkbox
{

	auto operator<<(ostream &out, const Abstract_Debug_Printable &to_be_debugged) noexcept -> ostream &
	{
		try
		{
			return to_be_debugged.print_debug_representation(out);
		}
		catch (...)
		{
			// Debug printing must not interfere with program execution
			return out;
		}
	}

	auto operator<<(ostream &out, Abstract_Debug_Printable const *const to_be_debugged) noexcept -> ostream &
	{
		try
		{
			if (nullptr == to_be_debugged)
			{
				return out << "nullptr";
			}
			else
			{
				return out << *to_be_debugged;
			}
		}
		catch (...)
		{
			// Debug printing must not interfere with program execution
			return out;
		}
	}

} // namespace junkbox
