/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/text.hpp"
#include <algorithm>
#include <cctype>

namespace junkbox::text
{

	using std::any_of;
	using std::begin;
	using std::cbegin;
	using std::cend;
	using std::end;
	using std::for_each;
	using std::transform;

	inline auto is_space(const char some_char) { return isspace(static_cast<unsigned char>(some_char)); }

	/*
	 * The last time I checked this worked with Clang but it did not with
	 * GCC:
	 *
	 *     return any_of(text.begin(), text.end(), isspace);
	 */
	auto contains_whitespace(const string &text) -> bool { return any_of(cbegin(text), cend(text), is_space); }

	auto escaped(const char char_to_escape, const string &text_to_be_escaped) -> string
	{
		string result;
		for (const auto curr_char : text_to_be_escaped)
		{
			if ('\\' == curr_char || char_to_escape == curr_char)
			{
				result += '\\';
			}
			result += curr_char;
		}
		return result;
	}

	auto escaped(const string_view chars_to_escape, const string &text_to_be_escaped) -> string
	{
		string result;
		for (const auto curr_char : text_to_be_escaped)
		{
			if ('\\' == curr_char || chars_to_escape.length() >= chars_to_escape.find(curr_char))
			{
				result += '\\';
			}
			result += curr_char;
		}
		return result;
	}

	auto double_quoted(const string &text_to_be_put_in_double_quotes) -> string
	{
		return '"' + escaped('"', text_to_be_put_in_double_quotes) + '"';
	}

	auto single_quoted(const string &text_to_be_put_in_single_quotes) -> string
	{
		return '\'' + escaped('\'', text_to_be_put_in_single_quotes) + '\'';
	}

	auto bracketed(const string &text_to_be_put_in_brackets) -> string
	{
		return '[' + escaped("[]", text_to_be_put_in_brackets) + ']';
	}

	auto starts_with(const string &text, const string_view prefix) -> bool { return 0 == text.rfind(prefix, 0); }

	auto upper_case(string text) -> string
	{
		transform(begin(text), end(text), begin(text), [](const auto c) { return toupper(c); });
		return text;
	}

	auto indented(const string &indentation, const string &text) -> string
	{
		string result{indentation};
		for_each(begin(text), end(text),
		         [&](const auto curr_char)
		         {
				 result.push_back(curr_char);
				 if ('\n' == curr_char)
				 {
					 result.append(indentation);
				 }
			 });
		return result;
	}

} // namespace junkbox::text
