/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "junkbox/base_exception.hpp"

namespace junkbox
{

	auto operator<<(ostream &out, const Base_Exception &ex) -> ostream & { return out << ex.pretty_message(); }

	auto operator<<(ostream &out, const Base_Exception *const ex) -> ostream &
	{
		return out << (nullptr == ex ? "nullptr" : ex->pretty_message());
	}

	Base_Exception::Base_Exception(const Message &message) : exception{}, m_message{message} {}

	auto Base_Exception::what() const noexcept -> const char * { return m_message.c_str(); }

	auto Base_Exception::message() const noexcept -> const Message & { return m_message; }

} // namespace junkbox
