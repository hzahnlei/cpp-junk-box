/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_debug_printable.hpp"
#include "assertion.hpp"
#include "base_exception.hpp"
#include "collection_builder.hpp"
#include "console_tracer.hpp"
#include "convenient_algorithms.hpp"
#include "crtp.hpp"
#include "deref_algorithms.hpp"
#include "dereferencing_iterator.hpp"
#include "files_and_dirs.hpp"
#include "junkbox/version.hpp"
#include "on_exit.hpp"
#include "os_error.hpp"
#include "rec_visit.hpp"
#include "text.hpp"
#include "todo_marker.hpp"

/**
 * @brief      My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * @details
 * # Motivation
 *
 * C++ is overwhelmingly full of great features - maybe too many features it seems occasionally. Some are not convenient
 * to use. For example: C++11 saw a new function `std::make_shared` but was lacking a `std::make_unique`.
 * `std::make_unique` was not available until the advent of C++14. `std::set` did not have a `contains` method before
 * C++20.
 *
 * # Solution
 *
 * The deficiencies mentioned above are not severe. But they are not really boosting productivity either. Therefore,
 * this library holds a collection of simple functions and data structures to make life as a C++ developer easier.
 *
 * # Counterindication
 *
 * Some of the deficiencies mentioned above have been solved in later versions of C++. In any case prefer standard C++
 * language and STL features over hand-crafted features.
 *
 * # Examples
 *
 * Definitely take a look at the test cases included herein. They are demonstrating the use of functions and data
 * structures of this library.
 *
 * @author     Holger Zahnleiter
 *
 * @copyright  MIT License, see repository LICENSE file
 */
namespace junkbox
{
} // namespace junkbox
