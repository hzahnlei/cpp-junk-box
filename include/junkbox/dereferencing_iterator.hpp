/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <cstddef>

namespace junkbox::deref
{

	using std::size_t;

	/**
	 * @brief Iterate over collections of smart pointers without giving up ownership.
	 *
	 * @details
	 * # Motivation
	 *
	 * When iterating collections of smart pointers, one gets access to the smart pointers. This is not so
	 * significant when using `std::shared_ptr` but especially when using `std::uniqe_ptr` one might (accidentally)
	 * take over ownership. That would corrupt the collection. And the object might be lost the moment the
	 * `std::unique_ptr` goes out of scope.
	 *
	 * # Solution
	 *
	 * This iterator iterates collections of smart pointers and dereferences them. The client of such iterators does
	 * not see smart pointers but constant references instead.
	 *
	 * This code is inspired by an article by Jonas Devlieghere:
	 * https://jonasdevlieghere.com/containers-of-unique-pointers/
	 *
	 * # Counterindication
	 *
	 * None.
	 *
	 * # Examples
	 *
	 * ```cpp
	 * const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	 *
	 * all_of(dereferencing_iterator(cbegin(some_numbers)),
	 *        dereferencing_iterator(cend(some_numbers)),
	          [](const auto &n){return n > 100;});
	 * ```
	 *
	 * With the dereferencing algorithms that is even more convenient (no need to use the iterator):
	 *
	 * ```cpp
	 * const vector<shared_ptr<int>> some_numbers{make_shared<int>(1), make_shared<int>(2), make_shared<int>(3)};
	 *
	 * deref::all_of(some_numbers,
	 *               [](const auto &n){return n > 100;});
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	template <class BASE_ITER> class Dereferencing_Iterator final : public BASE_ITER
	{
	    public:
		using Value_Type = typename BASE_ITER::value_type::element_type;
		using Pointer_Type = const Value_Type *;
		using Reference_Type = const Value_Type &;

		explicit Dereferencing_Iterator(const BASE_ITER &other) : BASE_ITER{other} {}

		[[nodiscard]] auto operator*() const -> Reference_Type { return *(this->BASE_ITER::operator*()); }

		[[nodiscard]] auto operator->() const -> Pointer_Type { return this->BASE_ITER::operator*().get(); }

		[[nodiscard]] auto operator[](const size_t index) const -> Reference_Type
		{
			return *(this->BASE_ITER::operator[](index));
		}
	};

	template <typename IT> [[nodiscard]] auto dereferencing_iterator(IT base_iter) -> Dereferencing_Iterator<IT>
	{
		return Dereferencing_Iterator<IT>(base_iter);
	}

} // namespace junkbox::deref
