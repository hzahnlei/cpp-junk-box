/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <functional>
#include <map>
#include <ostream>
#include <string>
#include <string_view>

namespace junkbox
{

	using std::function;
	using std::map;
	using std::ostream;
	using std::string;
	using std::string_view;

	/**
	 * @brief Simple functions for dealing with "text".
	 *
	 * @details
	 * # Motivation
	 *
	 * It is unbelievable what effort it takes to convert a string to uppercase in C++:
	 * ```cpp
	 * std::string s{"test"};
	 * std::transform(s.begin(), s.end(), s.begin(),
	 *                [](unsigned char c){ return std::toupper(c); });
	 * std::cout << s;
	 * // prints TEST
	 * ```
	 *
	 * # Solution
	 *
	 * The function herein a practicle small helpers to deal with text.
	 *
	 * # Counterindication
	 *
	 * In some cases it might be the better choice to use a library such as https://github.com/fmtlib/fmt.
	 *
	 * Prefer identical features from STL over my hand-crafted stuff.
	 *
	 * # Examples
	 *
	 * For example, one can convert a string to upper case like shown below. Compare that to the code from the
	 * introduction.
	 *
	 * ```cpp
	 * std::string s{"test"};
	 * std::cout << "original: " << s << ", uppercase: " << upper_case(s);
	 * // prints original: test, uppercase: TEST
	 * ```
	 *
	 * Of cause, the code from the introduction mutates the input. This might be more performant and less memory
	 * consuming. However, I prefer readability and immutability most of the time.
	 *
	 * @author Holger Zahnleiter
	 */
	namespace text
	{

		[[nodiscard]] auto contains_whitespace(const string &) -> bool;

		[[nodiscard]] auto escaped(const char char_to_escape, const string &text_to_be_escaped) -> string;
		[[nodiscard]] auto escaped(const string_view chars_to_escape, const string &text_to_be_escaped)
				-> string;

		[[nodiscard]] auto double_quoted(const string &) -> string;
		[[nodiscard]] auto single_quoted(const string &) -> string;
		[[nodiscard]] auto bracketed(const string &) -> string;

		[[nodiscard]] auto starts_with(const string &text, const string_view prefix) -> bool;

		[[nodiscard]] auto upper_case(string) -> string;

		[[nodiscard]] auto indented(const string &indentation, const string &text) -> string;

		using Quotation_Fun = function<string(const string &)>;

		[[nodiscard]] inline auto do_not_quote_elements(const string &to_be_quoted) { return to_be_quoted; }

		template <typename T, class Q = Quotation_Fun>
		auto representation_of_keys(const map<string, T> &key_val_map, const Q &quoted = single_quoted)
		{
			return [&](auto &out) -> auto &
			{
				auto i = 0U;
				for (const auto &entry : key_val_map)
				{
					if (i > 0)
					{
						out << ", ";
					}
					out << quoted(entry.first);
					i++;
				}
				return out;
			};
		}

		template <typename T, class Q = Quotation_Fun>
		auto representation_of_key_value_pairs(const map<string, T> &key_val_map,
		                                       const Q &quoted = single_quoted)
		{
			return [&](auto &out) -> auto &
			{
				auto i = 0U;
				for (const auto &entry : key_val_map)
				{
					if (i > 0)
					{
						out << ", ";
					}
					out << quoted(entry.first) << " -> " << entry.second;
					i++;
				}
				return out;
			};
		}

		template <typename T, class Q = Quotation_Fun>
		auto representation_of_elements(const T &list, const Q &quoted = single_quoted)
		{
			return [&](auto &out) -> auto &
			{
				auto i = 0U;
				for (const auto &element : list)
				{
					if (i > 0)
					{
						out << ", ";
					}
					out << quoted(element);
					i++;
				}
				return out;
			};
		}

	} // namespace text

	using Textual_Representation_Streamable = function<ostream &(ostream &out)>;

	inline auto operator<<(ostream &out, const Textual_Representation_Streamable &streamable) -> ostream &
	{
		return streamable(out);
	}

} // namespace junkbox
