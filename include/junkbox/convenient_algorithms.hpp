/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <algorithm>
#include <iterator>
#include <type_traits>

/**
 * @brief Convenient use of STL algorithms.
 *
 * @details
 * # Motivation
 *
 * STL algorithms are great. However, sometimes they are tedious to use. For example, most of the time you want
 * to iterate a complete collection. Writing
 * ```cpp
 * std::for_each(std::begin(collection), std::end(collection), [](){...});
 * ```
 * all the time is cumbersome.
 *
 * # Solution
 *
 * With this code herein you can achieve the same with less typing:
 * ```cpp
 * algo::for_each(collection, [](){...});
 * ```
 *
 * # Counterindication
 *
 * Use standard STL features from newer C++ versions when possible.
 * For example std::ranges::any_of etc.
 *
 * # Examples
 *
 * See above and the test cases.
 *
 * @deprecated This is at least parially deprecated as newer versions of C++ bring some of the stuff herein out of the
 * box.
 *
 * @author Holger Zahnleiter
 */
namespace junkbox::algo
{

	using std::all_of;
	using std::any_of;
	using std::begin;
	using std::cbegin;
	using std::cend;
	using std::decay_t;
	using std::declval;
	using std::end;
	using std::find;
	using std::for_each;
	using std::transform;

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] inline auto all_of(const ITERABLE &iterable, const PREDICATE &predicate) -> bool
	{
		return all_of(cbegin(iterable), cend(iterable), predicate);
	}

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] inline auto any_of(const ITERABLE &iterable, const PREDICATE &predicate) -> bool
	{
		return any_of(cbegin(iterable), cend(iterable), predicate);
	}

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] inline auto none_of(const ITERABLE &iterable, const PREDICATE &predicate) -> bool
	{
		return !any_of(cbegin(iterable), cend(iterable), predicate);
	}

	/**
	 * @deprecated Can use <tt>set::contains</tt> in C++20 instead. This is for convenience when using c++17 or
	 * older.
	 */
	template <class ITERABLE, class T>
	[[nodiscard]] inline auto contains(const ITERABLE &iterable, const T &value) -> bool
	{
		return find(cbegin(iterable), cend(iterable), value) != cend(iterable);
	}

	template <class ITERABLE, class ACTION> inline auto for_each(const ITERABLE &iterable, const ACTION &action)
	{
		return for_each(cbegin(iterable), cend(iterable), action);
	}

	template <class ITERABLE, class TRANSFORM> inline auto transform(ITERABLE &iterable, const TRANSFORM &mapping)
	{
		transform(begin(iterable), end(iterable), begin(iterable), mapping);
	}

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] auto copy_if(const ITERABLE &iterable, const PREDICATE &predicate)
	{
		using Result_Collection = decltype(decay_t<ITERABLE>(declval<ITERABLE>()));
		Result_Collection result;
		for (const auto &element : iterable)
		{
			if (predicate(element))
			{
				result.emplace_back(element);
			}
		}
		return result;
	}

	template <class ITERABLE, class T>
	[[nodiscard]] auto count_of_occurrences(const ITERABLE &iterable, const T &value)
	{
		auto result = 0U;
		for_each(iterable,
		         [&](const auto &element)
		         {
				 if (element == value)
				 {
					 result++;
				 }
			 });
		return result;
	}

} // namespace junkbox::algo
