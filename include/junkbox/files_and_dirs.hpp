/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <cstddef>
#include <stdexcept>
#include <string>

/**
 * @brief Small file helpers.
 *
 * @details
 * # Motivation
 *
 * I often write similar pieces of code when working with files and directories.
 *
 * Also, the style of C/C++ file handling functions is somewhat procedural and C oriented. For example using `char *`
 * instead of `std::string`.
 *
 * Status codes returned by functions are often ignored by the caller.
 *
 * # Solution
 *
 * This file contains file and directory functions I occasionally need. The code uses C++ types like `const std::string
 * &` instead of `char *`
 *
 * The functions are throwing exceptions in case of error. Exceptions cannot be ignored in contrast to return codes.
 * (However, return codes are still an option. But the functions should be `[[nodiscard]]` in this case.)
 *
 * # Counterindication
 *
 * None.
 *
 * # Examples
 *
 * It is actually just calling simple functions:
 * ```cpp
 * ...
 * if (file::directory_exists("./test")) {
 *     file::delete_all_content_from_directory("./test");
 * }
 * ...
 * ```
 *
 * Exceptions are thrown in case of error, in contrast to typical procedural functions where status codes are returned
 * and ignored by the caller:
 * ```cpp
 * ...
 * try {
 *     const auto text = file::load_text_file("test.txt");
 *     ...
 * } catch (const file::File_Error &cause) {
 *     std::cerr << "Failed to load text file: " << cause.what();
 * }
 * ...
 * ```
 *
 * @author Holger Zahnleiter
 */
namespace junkbox::file
{
	using std::runtime_error;
	using std::size_t;
	using std::string;

	using Directory_Name = string;
	using File_Name = string;

	auto delete_files_from_directory(const Directory_Name &) -> void;

	auto delete_all_content_from_directory(const Directory_Name &) -> void;

	[[nodiscard]] auto load_text_file(const File_Name &) -> string;

	[[nodiscard]] auto directory_exists(const Directory_Name &) -> bool;

	[[nodiscard]] auto file_count(const Directory_Name &) -> size_t;

	class File_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

} // namespace junkbox::file
