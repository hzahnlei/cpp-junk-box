/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "dereferencing_iterator.hpp"
#include <algorithm>
#include <iterator>

/**
 * @brief Use STL algorithms with collections of smart pointers.
 *
 * @details
 * # Motivation
 *
 * *  Often you iterate the complete collection. Hence writing `std::all_of(std::cbegin(x), std::cend(x), ...)` is
 * tedious. (See convenient algorithms.)
 * *  When applying STL algorithms on collections of `std::unique_ptr` there is the danger, that one takes over
 * ownership of the objects pointed to.
 *
 * # Solution
 *
 * *  The methods contained herein allow for a shorter writing, like so: `deref::all_of(x, ...)`.
 * *  Also a dereferencing itarator is used. When iterating a collection of smart pointers of objects of type `T`, the
 * dereferencing iterator gives you constant references of `T` instead of `std::unique_ptr<const T>`. So, predicate
 * functions can accept `const &` instead of `std::unique_ptr` and do not have to dereference. Furthermore the true
 * nature of the object gets hidden. The ownership cannot be taken over. In case of `std::shared_ptr` that is not so
 * important, but in case of `std::unique_ptr` it is.
 *
 * # Counterindication
 *
 * None.
 *
 * # Examples
 *
 * See above and corresponding test cases.
 *
 * @author Holger Zahnleiter
 */
namespace junkbox::deref
{

	using std::all_of;
	using std::any_of;
	using std::cbegin;
	using std::cend;
	using std::find;

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] inline auto all_of(const ITERABLE &iterable, const PREDICATE &predicate) -> bool
	{
		return all_of(dereferencing_iterator(cbegin(iterable)), dereferencing_iterator(cend(iterable)),
		              predicate);
	}

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] inline auto any_of(const ITERABLE &iterable, const PREDICATE &predicate) -> bool
	{
		return any_of(dereferencing_iterator(cbegin(iterable)), dereferencing_iterator(cend(iterable)),
		              predicate);
	}

	template <class ITERABLE, class PREDICATE>
	[[nodiscard]] inline auto none_of(const ITERABLE &iterable, const PREDICATE &predicate) -> bool
	{
		return !any_of(dereferencing_iterator(cbegin(iterable)), dereferencing_iterator(cend(iterable)),
		               predicate);
	}

	template <class ITERABLE, class T>
	[[nodiscard]] inline auto contains(const ITERABLE &iterable, const T &value) -> bool
	{
		return find(dereferencing_iterator(cbegin(iterable)), dereferencing_iterator(cend(iterable)), value) !=
		       dereferencing_iterator(cend(iterable));
	}

	template <class ITERABLE1, class ITERABLE2>
	[[nodiscard]] inline auto contains_all(const ITERABLE1 &iterable1, const ITERABLE2 &iterable2) -> bool
	{
		return all_of(dereferencing_iterator(cbegin(iterable2)), dereferencing_iterator(cend(iterable2)),
		              [&](const auto &element_from_iter2) { return contains(iterable1, element_from_iter2); });
	}

} // namespace junkbox::deref
