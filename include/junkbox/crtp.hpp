/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

namespace junkbox
{

	/**
	 * @brief Simplifies implementation of the CRTP pattern.
	 *
	 * @details
	 * # Motivation
	 *
	 * The ever same boilter plate code has to be implemented when implementing the CRTP pattern. Also, errors can
	 be made in the implementation.
	 *
	 * # Solution
	 *
	 * Use this as a base class when implementing the CRTP pattern. This is a simple class for your convenience.
	 * See https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/ for an introduction to the
	 * CRTP pattern.
	 *
	 * # Counterindication
	 *
	 * None.
	 *
	 * # Examples
	 *
	 * Take a look at the corresponding test cases for demonstrations of the CRTP pattern and use of this class.
	 *
	 * @author Holger Zahnleiter
	 */
	template <typename T, template <typename> class CRTP_TYPE> class Crtp
	{
	    public:
		[[nodiscard]] auto underlying() -> T & { return static_cast<T &>(*this); }

		[[nodiscard]] auto underlying() const -> T const & { return static_cast<const T &>(*this); }

	    private:
		Crtp() = default;
		friend CRTP_TYPE<T>;
	};

} // namespace junkbox
