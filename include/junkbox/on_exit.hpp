/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <utility>

namespace junkbox
{

	using std::exchange;
	using std::move;

	/**
	 * @brief Performs an action when exiting a function.
	 *
	 * @details
	 * # Motivation
	 *
	 * Often we need to make sure a certain action is performed at the end of a function. For example emmiting
	 * metrics, closing a file or freeing up memory.
	 *
	 * # Solution
	 *
	 * Another class that uses C++'s destructor guarantees to perform a last action when leaving a function or
	 * block.
	 *
	 * # Counterindication
	 *
	 * As I found out later, this is very similar to `final_action` from the Core Guideline Support Library. I
	 * recommend to use that one. See https://github.com/microsoft/GSL.
	 *
	 * # Examples
	 *
	 * The class can be used directly. And it is possible to have multiple instances in one block.
	 * ```cpp
	 * ...
	 * ostringstream out;
	 * ...
	 * On_Exit _{[&]() { out << "Done!"; }};
	 * ...
	 * ```
	 *
	 * One can also use a macro. This looks nicer but there can only be one macro call per block.
	 * ```cpp
	 * ...
	 * ostringstream out;
	 * ...
	 * ON_EXIT_DO(out << "Done!";);
	 * ...
	 * ```
	 *
	 * @author Holger Zahnleiter
	 *
	 * @deprecated Use `final_action` from the Core Guideline Support Library.
	 */
	template <class ACTION> class On_Exit final
	{

	    private:
		const ACTION m_action;
		const bool m_to_be_invoked = true;

	    public:
		On_Exit() = delete;

		explicit On_Exit(ACTION action) noexcept : m_action{move(action)} {}

		explicit On_Exit(On_Exit &) = delete;
		explicit On_Exit(const On_Exit &) = delete;

		On_Exit(On_Exit &&other) noexcept
				: m_action(move(other.m_action)),
				  m_to_be_invoked(exchange(other.m_to_be_invoked, false))
		{
		}

		~On_Exit() noexcept
		{
			if (m_to_be_invoked)
			{
				m_action();
			}
		}

		auto operator=(const On_Exit &) -> On_Exit & = delete;
		auto operator=(On_Exit &&) -> On_Exit & = delete;
	};

/*
 * Not recommended to use macros. However, nice syntax and less typing when using it. Cannot have multiple of these in
 * one block!
 */
#define ON_EXIT_DO(action)                                                                                             \
	const junkbox::On_Exit on_exit_do                                                                              \
	{                                                                                                              \
		[&]() { action; }                                                                                      \
	}

} // namespace junkbox
