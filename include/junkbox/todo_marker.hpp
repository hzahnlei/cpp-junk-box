/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <stdexcept>
#include <string>

namespace junkbox
{
	using std::runtime_error;

	/**
	 * @brief Indicates unfinished code during development phase.
	 * @details
	 * # Motivation
	 * During development phase not all branches are finished. I want to mark the places where I have to work on in
	 * order to finish my product.
	 * # Solution
	 * With this class, unfinished parts of the code are throwing an unambiguous exception. This reminds me, that
	 * there is still work to be done. I often prefer this over TODO comments. You just cant ignore an exception.
	 * # Counterindication
	 * This should not be part of the finished product. This is for development purposes only.
	 * # Examples
	 * Use the macro like so:
	 * ```cpp
	 * TODO();
	 * ```
	 * @author Holger Zahnleiter.
	 */
	class To_Do final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	/**
	 * @brief Indicates unsupported operations.
	 * @details
	 * # Motivation
	 * I want to mark the places in code where a certain operation is not supported. This might be the case in
	 * inheritence hierarchies. All classes of the hierarchy must implement all functions from the base class or
	 * interface. However, sometimes there is no meanungful answer a function can give.
	 * # Solution
	 * With this class, functions can be marked to not support an operation. I prefer this over comments. You just
	 * cant ignore an exception. # Counterindication It should seldom be the case that a function cannot deliver a
	 * meaningful answer or perform an desired action. This might be an indication of an design flaw in your class
	 * hierachy.
	 * # Examples Use the macro like so:
	 * ```cpp
	 * NOT_SUPPORTED();
	 * ```
	 * @author Holger Zahnleiter.
	 */
	class Not_Supported final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	/**
	 * @brief Indicates a runtime error.
	 * @details
	 * # Motivation
	 * Runtime errors can happen. I want a convenient way to throw a runtime exception. Furthermore, I want to know
	 * the location of where the problem occurred.
	 * # Solution
	 * This class in cooperation with a macro constructs a runtime exception. It also includes filename and line
	 * number in the error message.
	 * # Counterindication
	 * None
	 * # Examples
	 * ```cpp
	 * RUNTIME_ERROR("failed to open file");
	 * ```
	 * @author Holger Zahnleiter.
	 */
	class Runtime_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

} // namespace junkbox

#define TODO()                                                                                                         \
	throw junkbox::To_Do { std::string{"TODO in "} + __FILE__ + " at " + std::to_string(__LINE__) }

#define NOT_SUPPORTED()                                                                                                \
	throw junkbox::Not_Supported                                                                                   \
	{                                                                                                              \
		std::string{"Unsupported operation in "} + __FILE__ + " at " + std::to_string(__LINE__)                \
	}

/*
 * A convenient way for throwing a runtime exception including C++ file name and line number.
 */
#define RUNTIME_ERROR(message)                                                                                         \
	throw junkbox::Runtime_Error                                                                                   \
	{                                                                                                              \
		std::string{"Error in "} + __FILE__ + " at " + std::to_string(__LINE__) + ": " + message               \
	}
