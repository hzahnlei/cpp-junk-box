/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <concepts>
#include <memory>
#include <type_traits>
#include <utility>

namespace junkbox
{

	using std::decay_t;
	using std::derived_from;
	using std::enable_if_t;
	using std::forward;
	using std::is_same_v;
	using std::make_shared;
	using std::move;
	using std::shared_ptr;

	/**
	 * @brief `std::variant` and `std::visit` with recursive data structures.
	 *
	 * @details
	 * # Motivation
	 *
	 * `std::variant` and `std::visit` are very convenient tools. They do have their weak points though. I am
	 * especially talking about recursive structures here. Examples would be (parse) trees where nodes of different
	 * types may hold references to other nodes of various types. In such a case we could use `std::variant` to
	 * model the polymorphic nature of such noded. But, the references to the child nodes would have to be of that
	 * same variant type. This is a chicken and egg problem: We have a circular reference here.
	 *
	 * Good examples and explanations of `std::visit` and `std::variant` can be found here:
	 * https://www.cppstories.com/2018/06/variant/ and https://en.cppreference.com/w/cpp/utility/variant.
	 *
	 * # Solution
	 *
	 * I found a solution to the problem here
	 * https://www.youtube.com/watch?v=nNqiBasCab4&list=PLHTh1InhhwT45cqTfx5RbxxOXdpnlySVj&index=12 and here
	 * https://gist.github.com/tomoyanonymous/5f1b18d893d67f56ea0e2ab72ed142f3.
	 *
	 * The idea is to box/wrap types with circular reference so that the compiler does not complain when you define
	 * your variant, using incomplete (forward declared) types.
	 *
	 * # Counterindication
	 *
	 * None.
	 *
	 * # Examples
	 *
	 * Here is an outline of how you would use this template. The full code can be found in the corresponding and
	 * commented component test, also part of this repository.
	 *
	 * ```cpp
	 * struct Int_Literal final { int value; };
	 * struct Add_Op;
	 * struct Mul_Op;
	 * using Expr = variant<Int_Literal, Box<Add_Op>, Box<Mul_Op>>;
	 * struct Add_Op { Expr lhs; Expr rhs; };
	 * struct Mul_Op { Expr lhs; Expr rhs; };
	 *
	 * auto eval(const Expr &expression) -> int {
	 *     return visit(overloaded{[](const Int_Literal &literal) { return literal.value; },
	 *                             [](const Add_Op &op) { return eval(op.lhs) + eval(op.rhs); },
	 *                             [](const Mul_Op &op) { return eval(op.lhs) * eval(op.rhs); }},
	 *                  expression);
	 * };
	 *
	 * cout << eval(Expr{Add_Op{
	 *                          .lhs = Expr{Int_Literal{1}},
	 *                          .rhs = Expr{Mul_Op{
	 *                                             .lhs = Expr{Int_Literal{2}},
	 *                                             .rhs = Expr{Int_Literal{3}}}}}});
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	template <typename T> class Box
	{
	    private:
		shared_ptr<T> m_boxed_data;

	    public:
		Box() = delete;

		/*
		 * Recommendation is that constructors with one argument shall be marked "explicit". However, in the
		 * case of this class this is inconvenient as it forces us to use a lot of explicit boxes when
		 * constructing data structures. See
		 * https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rc-explicit.
		 */
		Box(shared_ptr<T> t) : m_boxed_data{t} {}

		Box(T rt) : m_boxed_data{make_shared<T>(move(rt))} {}

		template <typename U>
			requires is_same_v<decay_t<U>, T>
		Box(U &&rt) : m_boxed_data{make_shared<U>(forward<U>(rt))}
		{
		}

		operator T &() { return *m_boxed_data; }

		operator const T &() const { return *m_boxed_data; }

		auto unboxed() const -> T & { return *m_boxed_data; }
	};

	template <class... Ts> struct overloaded : Ts...
	{
		using Ts::operator()...;
	};

	template <class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

	/*
	 * Extract value of any type from variant and cast to a base type. Of cause, only works in case all types of the
	 * variant derive from the same base class.
	 */
	template <class T, class V> auto unboxed_as(const V &var) -> const T &
	{
		return visit(overloaded{[](const T &val) -> const T & { return val; }}, var);
	}

	template <class... Ts> struct overloaded_rec : Ts...
	{
		using Ts::operator()...;

		template <typename T> decltype(auto) operator()(Box<T> a) const { return (*this)(a.unboxed()); }

		template <typename T>
			requires derived_from<decay_t<T>, Box<T>>
		decltype(auto) operator()(T &&a) const
		{
			return (*this)(forward<decltype(a)>(a.unboxed()));
		}
	};

	template <class... Ts> overloaded_rec(Ts...) -> overloaded_rec<Ts...>;

} // namespace junkbox
