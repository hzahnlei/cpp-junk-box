/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <string>

namespace junkbox
{
	using std::string;

	using User_Message = string;
	using OS_Message = string;

	/**
	 * @brief Nice error message for operating system errors.
	 *
	 * @details
	 * # Motivation
	 *
	 * I want uniform error messages when using OS system calls. With error code and OS error message. I also want
	 * the ability to pass a "user message" to express what was the intention that caused the error.
	 *
	 * # Solution
	 *
	 * The function presented herein formats the error code (`errno`) and message from the operating system into a
	 * nicely formatted error message. Finally it attaches a "user message". This message should express what the
	 * action, taht lead to the error, intended to achieve.
	 *
	 * # Counterindication
	 *
	 * None.
	 *
	 * # Examples
	 *
	 * Assume the file `test.txt` does not exist:
	 * ```cpp
	 * ...
	 * ifstream some_file{"test.txt"};
	 * if (errno != 0) {
	 *     throw runtime_error{os_error_message("test.txt")};
	 * }
	 * ...
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	[[nodiscard]] auto os_error_message(const User_Message &) -> OS_Message;

} // namespace junkbox
