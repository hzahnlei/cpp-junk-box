/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "base_exception.hpp"
#include <cstddef>
#include <string>

namespace junkbox
{

	using std::size_t;
	using std::string;

	/**
	 * @brief An exception class to signal violation of assertions.
	 *
	 * @details
	 * # Motivation
	 *
	 * The terms "sanity check" and "assertion" are used synonymously below. Herein, these terms do not refer to
	 * checks at the level of interfaces at an application's outer rim, where incoming requests are validated.
	 * Sanity check or assertion here does refer to checks deep inside your own code where your own functions are
	 * making sure they are called with proper arguments.
	 *
	 * It is my observation though that most such sanity checks or assertions are actually never triggered in
	 * reality. These might be some of the reasons:
	 * *  Maybe the ckecks were too paranoid in the first place?
	 * *  Why should you call your own functions with improper arguments?
	 * *  After a development phase, that includes extensive testing, most programming errors are fixed. Violations
	 * of assumptions simply do not occur.
	 *
	 * But, sanity checks can have effects:
	 * *  They are polluting your code, making it harder to recognize what is essential for a function.
	 * *  The use run-time. (They are wasting it in case your program is well tested and they never trigger.)
	 *
	 * # Solution
	 *
	 * The makro and exception class presented in this file are intended to uncover programming arrors during the
	 * development phase only. They are only effective when compiled with debug information. The release version
	 * will not contain those checks anymore.
	 * *  To activate the assertion, the compiler definition `NDEBUG` has to be `true` (aka `!= 0`). This is the
	 * case for release builds.
	 * *  To deactivate the assertion, the compiler definition `NDEBUG` has to be `false` (aka `== 0`). This is the
	 * case for debug builds.
	 *
	 * # Counterindication
	 *
	 * I am trying not to blight my code by extensive use of assertions. In fact, I am using only a few assertions
	 * here and there at some critical parts of the code.
	 *
	 * In general I do validations at the outer rim of my applications or libraries. This outer defense perimeter
	 * might be REST controllers, where I am receiving messages from god knows whom. Or, it might be the public
	 * functions of libraries/APIs. However, I abstain from using assertions between the inner layers of my
	 * applications or libraries.
	 *
	 * # Examples
	 *
	 * ```cpp
	 * auto print(string const * const value) -> void {
	 *     DEV_ASSERT(value != nullptr, "missing value");
	 *     cout << "value=" << *value;
	 * }
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	class Assertion_Violation final : public Base_Exception
	{
	    public:
		using File_Name = string;
		using Line_Number = size_t;

	    private:
		const File_Name m_file;
		const Line_Number m_line;

	    public:
		Assertion_Violation() = delete;
		Assertion_Violation(const File_Name &, const Line_Number, const Message &);
		explicit Assertion_Violation(const Assertion_Violation &) = delete;

		auto operator=(const Assertion_Violation &x) -> Assertion_Violation & = delete;
		auto operator=(Assertion_Violation &&) -> Assertion_Violation & = delete;

		[[nodiscard]] auto pretty_message() const noexcept -> Message override;

		[[nodiscard]] auto file() const noexcept -> const File_Name &;
		[[nodiscard]] auto line() const noexcept -> Line_Number;
	};

} // namespace junkbox

// This could be  made a function using  std::source_location.  However,  C++20
// support is not complete in the current versions of Clang and GCC.  This will
// be reworked in a future version of this library.
#ifdef NDEBUG
#define DEV_ASSERT(condition, message) ((void)0)
#else
#define DEV_ASSERT(condition, message)                                                                                 \
	if (!(condition))                                                                                              \
	{                                                                                                              \
		throw junkbox::Assertion_Violation{__FILE__, __LINE__, message};                                       \
	}
#endif
