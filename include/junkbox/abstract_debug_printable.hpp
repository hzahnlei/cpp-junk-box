/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <ostream>
#include <string>

namespace junkbox
{

	using std::ostream;
	using std::string;

	/**
	 * @brief Classes implementing this, can be "debug printed" onto streams.
	 *
	 * @details
	 * # Motivation
	 *
	 * *  Debugging is my preferred way to analyse program flow and data when analysing bugs and incidents. However,
	 * using a debugger is often not an option.
	 * *  Furthermore the way complex data is presented by the debugger is often hard to read. Especially when
	 * compared to Java data structures, for example.
	 * *  In such cases it can be quite helpful to print or log a nice and human readable representation of that
	 * data.
	 *
	 * # Solution
	 *
	 * Have your classes derive from this abstract class in case you want them to be "printable" or "loggable". A
	 * `<<` operator is readily provided.
	 *
	 * You only have to implement the abstract method declared herein, besides deriving from this class. In any case
	 * make sure the printed representation is human readable and meaningful. Otherwise it would not really support
	 * the debuging activity. Representations can be established ones such as JSON but custom representations are
	 * fine as long they are easy to read.
	 *
	 * Notice the `indentation` parameter. It can be used to achieve indentet representation in case of nested data
	 * structures. You may simply ignore it, if you do not want to indent. Otherwise call
	 * `x.print_debug_representation(out, indentation+'\t');` to increase the depth of the indentation. Of cause you
	 * can also use a number of whitespacce characters if you do not want to use tab characters for indentation.
	 *
	 * **Attention!** Logging and "debug printing" should never interfere with normal program execution. Therefore,
	 * the readily provided `<<` operator catches all exceptions that might leak from your implementation of
	 * `print_debug_representation`. You should never directly call `Abstract_Debug_Printable` on the root object of
	 * an hierarchy to be printed but use the `<<` operator instead. Your implementation of
	 * `Abstract_Debug_Printable` can of cause call `Abstract_Debug_Printable` for its subobjects directly.
	 *
	 * # Counterindication
	 *
	 * I am recommending to use a function for printing instead of inheriting the ability to "debug print". This
	 * is less invasive and more flexible. One reason to use inheritance though would be the ability to access
	 * protected/private members.
	 *
	 * # Examples
	 *
	 * The following example shows the fictitious class `My_Struct` and mentions another fictitious class
	 * `My_Other_Struct`. Both are supposed to derive from `Abstract_Debug_Printable`. An example implementation of
	 * `print_debug_representation` is shown. `My_Struct::print_debug_representation` is calling
	 * `My_Other_Struct::print_debug_representation`. It increases the indentation depth by concatenating to white
	 * space characters.
	 *
	 * ```cpp
	 * struct My_Struct : public Abstract_Debug_Printable {
	 *     ...
	 *     vector<My_Other_Struct> members;
	 *     ...
	 *     auto print_debug_representation(ostream &out, const string &indentation = "") const override -> ostream &
	 *     {
	 *         out << "[\n";
	 *         for (const auto &member : members) {
	 *             out << indentation + "  ";
	 *             member.print_debug_representation(out, indentation + "  ");
	 *             out << '\n';
	 *         }
	 *         out << "]";
	 *         return ostream;
	 *     }
	 *     ...
	 * };
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	class Abstract_Debug_Printable
	{

	    public:
		virtual ~Abstract_Debug_Printable() = default;

		friend auto operator<<(ostream &, const Abstract_Debug_Printable &) noexcept -> ostream &;

		friend auto operator<<(ostream &, Abstract_Debug_Printable const *const) noexcept -> ostream &;

	    private:
		/**
		 * @brief The actual printing! Requires individual implementation for each derived class.
		 *
		 * @details Derived classes have to implement this. Implementors have to make sure that this methods
		 * prints a human readable representation of this object. Indentation can be used to achieve
		 * readability, but it is an option. Just ignore the <tt>indentation</tt> argument, if you do not want
		 * to make use of it.
		 *
		 * @param out Output to this stream.
		 * @param indentation The level of indentation. Each new line should start with this indentation.
		 * Recursive calls can indent deeper by passing <tt>indentation + '\t'</tt>. Instead of a tab character
		 * one can also increase indentation by a number of spaces, of cause.
		 * @return ostream& The stream for chaining printing operations.
		 */
		virtual auto print_debug_representation(ostream &, const string &indentation = "") const
				-> ostream & = 0;
	};

} // namespace junkbox
