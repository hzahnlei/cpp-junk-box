/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <utility>
#include <vector>

/**
 * @brief Conveniently build collections.
 *
 * @details
 * # Motivation
 *
 * I want to initialize collections in one statement. I want to avoid multi-stage initialization and I want data to be
 * immutable whenever possible.
 *
 * This multi-stage initialization of a vector is neither brief nor immutable:
 * ```cpp
 * vector<unique_ptr<string>> some_list;
 * some_liste.emplace_back(make_unique<string>("Hello"));
 * some_liste.emplace_back(make_unique<string>("World"));
 * ```
 *
 * # Solution
 *
 * This file contains initialization functions that allow to construct (immutable) collections with one function call.
 *
 * I am mainly using these function in test cases to construct test data.
 *
 * # Counterindication
 *
 * Prefer the initializer syntax of more modern C++ version, if available.
 *
 * # Examples
 *
 * ```cpp
 * const auto some_list = collection::as_vector(make_unique<string>("Hello"),
 *                                              make_unique<string>("World"));
 * ```
 *
 * @deprecated The initializer syntax of more modern C++ versions allow initialization of collections in a compact way.
 * There is no need to use the functions contained herein anymore. These functions might be helpful in older versions of
 * C++ though.
 *
 * @author Holger Zahnleiter
 */
namespace junkbox::collection
{

	using std::forward;
	using std::move;
	using std::vector;

	template <typename T> [[nodiscard]] auto as_vector(const T &element) -> vector<T>
	{
		vector<T> result;
		result.emplace_back(element);
		return result;
	}

	template <typename T> [[nodiscard]] auto as_vector(T &&element) -> vector<T>
	{
		vector<T> result;
		result.push_back(forward<T>(element));
		return result;
	}

	namespace detail
	{
		template <typename T> auto add_to_vector(vector<T> &result, const T &element) -> void
		{
			result.emplace_back(element);
		}

		template <typename T> auto add_to_vector(vector<T> &result, T &&element) -> void
		{
			result.push_back(forward<T>(element));
		}

		template <typename T, typename... Ts>
		auto add_to_vector(vector<T> &result, const T &element, const Ts &...elements) -> void
		{
			result.emplace_back(element);
			(result.emplace_back(elements), ...);
		}

		template <typename T, typename... Ts>
		auto add_to_vector(vector<T> &result, T &&element, Ts &&...elements) -> void
		{
			result.push_back(forward<T>(element));
			(result.push_back(forward<Ts>(elements)), ...);
		}

	} // namespace detail

	template <typename T, typename... Ts>
	[[nodiscard]] auto as_vector(const T &element, const Ts &...elements) -> vector<T>
	{
		vector<T> result;
		detail::add_to_vector(result, element, elements...);
		return result;
	}

	template <typename T, typename... Ts> [[nodiscard]] auto as_vector(T &&element, Ts &&...elements) -> vector<T>
	{
		vector<T> result;
		detail::add_to_vector(result, forward<T>(element), forward<Ts>(elements)...);
		return result;
	}

} // namespace junkbox::collection
