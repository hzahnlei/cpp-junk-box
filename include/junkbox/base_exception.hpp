/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <exception>
#include <ostream>
#include <string>

namespace junkbox
{

	using std::exception;
	using std::ostream;
	using std::string;

	/**
	 * @brief An exception class to provide more comprehensive error messages.
	 *
	 * @details
	 * # Motivation
	 *
	 * Usually, exceptions are little more than wrappers for text messages, explaining the error that has raised
	 * the exception. In some cases such messages are not sufficient to understand the problem. Additional
	 * information would be helpful in analyzing the cause of error.
	 *
	 * Furthermore, raising exceptions should not eclipse the root cause. Another exception might be thrown, while
	 * we format a complex error message with more details. That exception could then eclipse the actual error.
	 *
	 * Having extra data members for additional information has another advantage over just holding a text message:
	 * Such additional data can potentially be evaluated automatically.
	 *
	 * # Solution
	 *
	 * This exception class is indended as a base class for exceptions that carry more details beyond an error
	 * message:
	 * *  The error message can be accessed with the `what` functions, as we are used to.
	 * *  Additional details may be held in additional data members.
	 * *  The abstract `pretty_message` function can be overwritten to produce a complex error message, that is made
	 * up of the message passed in on construction plus text representation of the additional details. The function
	 * is `noexcept` and should not throw another exception while formatting the pretty message.
	 *
	 * Derivatives of this class can be printed onto streams. A readily provided stream operator takes care of this.
	 *
	 * # Counterindication
	 *
	 * Prefer C++ exceptions from the STL, if you do not need additional data members.
	 *
	 * # Examples
	 *
	 * Derivatives of this class usually hold further data members in addition to the message.
	 * Examples might be line number and column, where the error occurred. This can be used to produce error
	 * messages that are more helpful for the developer.
	 *
	 * ```cpp
	 * class My_Exception final : public Base_Exception {
	 *   private:
	 *     const size_t m_line;
	 *     ...
	 *   public:
	 *     My_Exception(const size_t, const string &)...
	 *     ...
	 *     auto pretty_message() const noexcept -> string override {
	 *         try {
	 *             return m_message + " at line " + m_line;
	 *         } catch (...) {
	 *             return "";
	 *         }
	 *     }
	 *     ...
	 * };
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	class Base_Exception : public exception
	{

	    public:
		using Message = string;

	    private:
		const Message m_message;

	    public:
		Base_Exception() = delete;
		explicit Base_Exception(const Message &);
		explicit Base_Exception(Base_Exception &) = delete;
		explicit Base_Exception(const Base_Exception &) = delete;

		auto operator=(const Base_Exception &) -> Base_Exception & = delete;
		auto operator=(Base_Exception &&) -> Base_Exception & = delete;

		[[nodiscard]] auto what() const noexcept -> const char * final;
		[[nodiscard]] auto message() const noexcept -> const Message &;
		[[nodiscard]] virtual auto pretty_message() const noexcept -> Message = 0;
	};

	auto operator<<(ostream &, const Base_Exception &) -> ostream &;
	auto operator<<(ostream &, const Base_Exception *const) -> ostream &;

} // namespace junkbox
