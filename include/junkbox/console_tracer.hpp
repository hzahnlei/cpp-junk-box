/*
 * My C++ Junk Box - Small C++ helpers that come in handy every now and then.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <ostream>
#include <string>

namespace junkbox
{

	using std::ostream;
	using std::string;

	/**
	 * @brief Helps you tracking program flow through your code.
	 *
	 * @details
	 * # Motivation
	 *
	 * Sometimes using a debugger is not an option when analyzing problems. It would be nice to have a way to see
	 * the flow through the code while the program is executed.
	 *
	 * # Solution
	 *
	 * The macro presented herein helps in tracking the program flow through your code. Place `TRACE(std::cout);` as
	 * the first statement of your functions. The tracer will then print entry and exit of every function that is
	 * called and that uses the tracer. Even in case of exceptions.
	 *
	 * The implementation uses the destructor guarantees. The macro instanciates a variable. This prints the entry
	 * message. The exit message is printed when the function exits (either normal or exception). At that point in
	 * time the destructor gets called and the exit message gets printed.
	 *
	 * # Counterindication
	 *
	 * This is meant as a developer tool. It should not go into the final production version of your code. Use it
	 * to follow the program flow when using a debugger is no option.
	 *
	 * # Examples
	 *
	 * ```cpp
	 * auto my_function() -> void {
	 *     TRACE(std::out);
	 *     ...
	 * }
	 * ```
	 *
	 * @author Holger Zahnleiter
	 */
	class Console_Tracer final
	{

	    public:
		using Trace_Message = string;

	    private:
		ostream &m_out;
		const Trace_Message m_pretty_function;

	    public:
		Console_Tracer(ostream &, const char *const);
		~Console_Tracer();
	};

#define TRACE(out)                                                                                                     \
	_Pragma("GCC diagnostic push") _Pragma("GCC diagnostic ignored \"-Wshadow\"")                                  \
			junkbox::Console_Tracer console_tracer{out, __PRETTY_FUNCTION__};                              \
	_Pragma("GCC diagnostic pop")

} // namespace junkbox
