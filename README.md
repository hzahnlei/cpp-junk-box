# My C++ Junk Box

Small C++ helpers that come in handy every now and then.

<img src="artwork/logo256x256.png" hight="96" width="96"/>

## What's in the Box?

It's small C++ helper classes and functions:

*  Sometimes I just do not want to tie in a full blown library just because I need some general functionality.
    Therefore I am collecting handy functions and classes herein.
*  Often I am wrapping existing functionality in small functions to give it a name - for better readability ultimately.
    I am doing this so often, that I have decided to wrap this here once and for all.

I am just giving some examples for what functionality is delivered by this library.
For details just look at the code and accompanying unit tests.

* **Text**
    *  `contains_whitespace` checks for strings to contain whitespace.
        Do not want to repeat my self and always type `any_of(begin(some_text), end(some_text), isspace)`.
        It is just more convenient to type `contains_whitespace(some_text)`.
        Also, I consider this to be more readable.
    *  `escaped` escapes characters in a string.
        (And also escapes the escape symbol.)
    *  `single_quoted`, `bracketed` etc. surround texts with quotes, brackets etc.
        (And also escapes the delimiting characters.)
*  **RAII**
    *   `On_Exit` RAII wrapper can be used to have an action berformed at the end of a function (scope).
        No matter if the scope was left normally or abnormally.
        Very similar to Core Guideline Support Library `final_action` (https://github.com/microsoft/GSL).
*  **CRTP**
    *   A template for deriving templates that implement the CRTP idom.
    *   Contains a method for downcasting.
        I consider `underlying().some_function()` more expressive/readable than `static_cast<const T &>(*this).some_function()`
    *   Hides the default constructor so that instances can only be created by templates deriving this template.
*  **Developer stuff**
    *  `DEV_ASSERT` is a assertion that is only part of debug builds but will not be present in release builds.
        (Which should not be a problem since you are heavily testing your code.)
        Violation of an assertion causes an exception to be thrown.
        This assertion violation exception features a relatively nice error message, with file name, and line number of tle location where the violation occurred.
        Furthermore you can add an explanatory message.
    *   `TODO` can be used to mark branches of the code that are not done yet.
        I am developing in a test-driven style.
        Things come into existence incrementally.
        `TODO` reminds you with an `runtime_exception` you cannot overlook.
*  **Algorithms**
    *  Standard algorithms are great.
        But sometimes I am tired of typing so many `begin`s and `end`s like in `for_each(begin(some_collection), end(some_collection), some_lambda)`.
        `for_each`'s interface is flexible. But most of the time I do not make use of that flexibility.
        I am iterating over the complete collections most of the time. Hence, for me it is more convenient to type `for_each(some_collection, some_lambda)`.
        Included herein are simple functions that wrap the standard algorithms I am using more frequently for simple use.
*  **Miscellaneous**
    *  For more small stuff just open the junk box and see what is in there.

## Project structure

| File             | Content |
|------------------|---------|
| `.clang-format`  | Configuration for the Clang formatting tool used to format the source code. |
| `.gitlab-ci.yml` | Build automation for https://gitlab.com. |
| `CMakeLists.txt` | Top level, hand crafted CMake file. Generated CMake files will be generated in the `build` folder. |
| `conanfile.txt`  | Libraries, this library depends upon. This is just the Catch2 testing framework. No other dependencies in this case. |
| `Makefile`       | Hand crafted make file used for convenient local/manual builds as well as automated CI builds. |
| `artwork/`       | Affinity Designer file for the project logo. |
| `build/`         | Created by build script, not checked in. Contains temporary, build-related files. |
| `include/`       | Contains all headers of the C++ Junk Box library. |
| `model/`         | Models to generate code from. |
| `packaging/`     | Conan receipt for packaging this library. Further files will created automatically when packaging. These are not checked in. |
| `src/`           | Contains all sources of the C++ Junk Box library. |
| `test/`          | Unit and component tests (based on Catch2). |

## Building/Testing

### Locally/Command Line

The acompanying `Makefile` contains targets for (almost) anything.
Once you have checked out this prject you can build it like so:

- `make dependencies` will download all the dependencies and have Conan produce a CMake configuration (preset) to work with.
- `make build` will then compile the library.
- `make test` runs all the tests
- You can also run `make clean dependencies build test` in one piece.

### IDE (Visual Studio Code)

Just execute `make code`.
This will picks the CMake preset (generated by `make dependencies`) and starts VS Code.
VS Code will be configured to allow for

- Building source and tests
- Code completion
- Debuggung
- Executing tests

### CI

See `.gitlab-ci.yml` for details.

## Using This Lib In Your Projects

GitLab`s Conan package registry currently does not support Conan 2.0.
Therefore you cannot reference the package from the registry.
Instead you need to build the library and push it to your local Conan registry.

So, this is how you could add this library to your project (assuming you are using Conan as a dependency manager):

-  First download, build and push that library to your local Conan registry
   ```shell
   git clone https://gitlab.com/hzahnlei/cpp-conan-cmake-templates.git
   cd cpp-conan-cmake-templates
   # Always suggest to explicitely pick a version
   git checkout 1.7.2
   make package
   ```
-  Add a dependency like so to your `conanfile.txt`
   ```shell
   [requires]
   junkbox/1.7.2
   ```
-  Now build your own application or library.
   The dependency is now satisfied from your local Conan registry.

The process, described above, can be used for local development environments as well as for automated CI builds.

## Acknowledgments

This library is built on top of and by use of many opensource or free of charge libraries, tools and services.
Thank you very much:

*  [Visual Studio Code](https://code.visualstudio.com)
*  [C++ Extension for VSC](https://github.com/Microsoft/vscode-cpptools.git)
*  [CMake Extension for VSC](https://github.com/microsoft/vscode-cmake-tools)
*  [Test Extension for VSC](https://github.com/matepek/vscode-catch2-test-adapter.git)
*  [CMake](https://cmake.org)
*  [GNU Make](https://www.gnu.org/software/make/)
*  [Conan](https://conan.io)
*  [GCC](https://gcc.gnu.org)
*  [Clang](https://clang.llvm.org)
*  [GitLab](https://gitlab.com)
*  [Bintray](https://bintray.com/hzahnlei)
*  [Catch2](https://github.com/catchorg/Catch2)
*  [cppcheck](https://sourceforge.net/projects/cppcheck/)
